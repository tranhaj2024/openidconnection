package com.savis.ttdt.endpoint.rest;

import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.common.dto.SearchForm;
import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import com.savis.ttdt.endpoint.dto.common.JsonHelper;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentGatesMerchantsService;
import com.savis.ttdt.service.PaymentLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/savis/lgsp")
public class PaymentGatesMerchantResource {

    @Autowired private PaymentGatesMerchantsService paymentGatesMerchantsService;
    @Autowired private PaymentLogService paymentLogService;

    @GetMapping(value="/payGatesMerchants", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public @ResponseBody ResponseData<Page<PaymentGatesMerchants>> getPage( Pageable pageable){
        ResponseData<Page<PaymentGatesMerchants>> response = new ResponseData<>();

        Page<PaymentGatesMerchants> pay = null;

        try {
            pay = paymentGatesMerchantsService.getPage(pageable);
            response.setData(pay);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);
        } catch (Exception e) {
            // TODO: handle exception
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/list/searchLogin", method = RequestMethod.GET)
    public @ResponseBody ResponseData<Page<Object>> searchLogin(@RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<Object>> response = new ResponseData<>();
        SearchForm searchForm = JsonHelper.jsonToObject(search, SearchForm.class);
        Page<Object> pay =null ;
        try {
            pay = paymentLogService.getLogLogin(searchForm, pageable);
            response.setData(pay);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);
        } catch (Exception e) {

            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

}
