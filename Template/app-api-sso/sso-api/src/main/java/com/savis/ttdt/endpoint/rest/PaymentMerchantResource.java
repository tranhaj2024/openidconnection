package com.savis.ttdt.endpoint.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentMerchants;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentGatesService;
import com.savis.ttdt.service.PaymentMerchantsService;

@RestController
@RequestMapping("/api/v1/savis/lgsp/")
public class PaymentMerchantResource {
	
	@Autowired
	private PaymentMerchantsService paymentMerchantsService;
	
	@GetMapping(value="payMerchants", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentMerchants>> getPage(Pageable pageable){
		ResponseData<Page<PaymentMerchants>> response = new ResponseData<>();
		
		Page<PaymentMerchants> pay = null;
		
		try {
			pay = paymentMerchantsService.getPage(pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@GetMapping(value="payMerchants/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Optional<PaymentMerchants>> getOne(@PathVariable("id") int id){
		ResponseData<Optional<PaymentMerchants>> response = new ResponseData<>();
		
		Optional<PaymentMerchants> pay = null;
		
		try {
			pay = paymentMerchantsService.findOne(id);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@PostMapping(value = "/payMerchants", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> create(@RequestBody PaymentMerchants paymentGates) {
		ResponseData<Integer> response = new ResponseData<>();

		int apps = -1;

		try {
			apps = paymentMerchantsService.create(paymentGates);
			if(apps == Constants.SUCCESS_CODE_FIELD_UNEXIST) {
				response.setData(Constants.SUCCESS_CODE_FIELD_UNEXIST);
			} else {
				throw new Exception();
			}

			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}

		return response;
	}
	
	@RequestMapping(value = "/payMerchants/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> deleteMultiple(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		
//		List<PaymentMerchants> apps = new ArrayList<PaymentMerchants>();
//		for (int i = 0; i < entityIds.length; i++) {
//			apps.add(paymentMerchantsService.findOne(entityIds[i]));
//		}
		
		int result = paymentMerchantsService.delete(id);
		if (result == 1) {
			response.setData(result);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} else {
			response.setData(0);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@PutMapping(value = "/payMerchants", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> update(@RequestBody PaymentMerchants paymentGates) {
		ResponseData<Integer> response = new ResponseData<>();

		int apps = -1;

		try {
			apps = paymentMerchantsService.update(paymentGates);
			if(apps == Constants.SUCCESS_CODE_FIELD_UNEXIST1) {
				response.setData(Constants.SUCCESS_CODE_FIELD_UNEXIST1);
			} else {
				throw new Exception();
			}

			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}

		return response;
	}
	
	@PostMapping(value="payMerchants/search", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentMerchants>> search(@RequestBody PaymentMerchants paymentMerchants, Pageable pageable){
		ResponseData<Page<PaymentMerchants>> response = new ResponseData<>();
		
		Page<PaymentMerchants> pay = null;
		
		try {
			pay = paymentMerchantsService.search(paymentMerchants, pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
}
