package com.savis.ttdt.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "payment_gates")
@Getter
@Setter
@Data
@AllArgsConstructor
public class PaymentGates {
	@Id
	@TableGenerator(name = "gen_id", table = "hibernate_gen_id", pkColumnName = "gen_name", valueColumnName = "gen_value", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gen_id")
	@Column(name = "GateId")
	private int gateId;
	@Column(name = "GateName")
	private String gateName;
	@Column(name = "GateCode")
	private String gateCode;
	@Column(name = "Status")
	private int status;
	@Column(name = "Note")
	private String note;
	@Column(name = "InitUrl")
	private String initUrl;
	@Column(name = "QueryUrl")
	private String queryUrl;
	@Column(name = "doisoat")
	private String doiSoat;
	
	
	public PaymentGates() {
		super();
	}
	
	
}
