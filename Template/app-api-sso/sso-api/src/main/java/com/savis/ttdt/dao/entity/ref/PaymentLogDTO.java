package com.savis.ttdt.dao.entity.ref;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@Data
public class PaymentLogDTO {
	
	private int id;
	
	private String MaLoi;
	
	private String MoTaLoi;
	
	private String UrlThanhToan;
	
	private String MaXacThuc;
	
	private String NoiDungHoaDon;
	
	private String UrlBienLai;
	
	private String binary;

	public PaymentLogDTO() {
		super();
	}
	
}
