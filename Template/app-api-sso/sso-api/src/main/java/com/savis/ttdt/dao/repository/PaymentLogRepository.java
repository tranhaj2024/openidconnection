package com.savis.ttdt.dao.repository;

import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import org.springframework.data.jpa.repository.JpaRepository;

import com.savis.ttdt.dao.entity.PaymentLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentLogRepository extends JpaRepository<PaymentLog, Integer>, JpaSpecificationExecutor<PaymentLog> {

}
