package com.savis.common.config;

import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "ttdtEntityManagerFactory", transactionManagerRef = "ttdtTransactionManager", basePackages = "com.savis.ttdt.dao.repository")
public class TtdtDatasourceConfig {
	@Bean
	@ConfigurationProperties("app.ttdt.datasource")
	public DataSourceProperties ttdtDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "ttdtsDataSource")
	public HikariDataSource ttdtDataSource() {
		return ttdtDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "ttdtEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean ttdtEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("commonJpaProperties") Map<String, String> jpaProperties) {
		return builder.dataSource(ttdtDataSource()).packages("com.savis.ttdt.dao.entity")
				.persistenceUnit("ttdt-db").properties(jpaProperties).build();
	}

	@Bean(name = "ttdtTransactionManager")
	public PlatformTransactionManager ttdtTransactionManager(
			@Qualifier("ttdtEntityManagerFactory") EntityManagerFactory ttdtEntityManagerFactory) {
		return new JpaTransactionManager(ttdtEntityManagerFactory);
	}
}