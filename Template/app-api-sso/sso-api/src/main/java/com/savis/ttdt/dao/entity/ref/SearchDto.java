package com.savis.ttdt.dao.entity.ref;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchDto {
	
	/** end data of the search range */
	private String endDate;
	/** end data of the search date format */
	private String startDate;
	
	private String subject;
	private String content;
	private String businessDocReason;
	private String codeCodeNumber;
//	private String promulgationInfoDate;
	private String businessDocType;
	private int isSended;
	private String organization_name;
	private int checkStatus;
	
	
}
