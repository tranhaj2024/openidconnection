package com.savis.ttdt.dao.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentMerchants;

public class PaymentMerchantSpecifications {
	public static Specification<PaymentMerchants> advanceFilter(PaymentMerchants pay) {
		return new Specification<PaymentMerchants>() {

			@Override
			public Predicate toPredicate(Root<PaymentMerchants> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;
				
				if(pay == null) {
					return null;
				}
				if (pay.getMerchantName() != null && !pay.getMerchantName().equals("")) {
					obj = cb.like(cb.lower(root.get("merchantName")), "%" + CommonUtils.standardized(pay.getMerchantName().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getMerchantCode() != null && !pay.getMerchantCode().equals("")) {
					obj = cb.like(cb.lower(root.get("merchantCode")), "%" + CommonUtils.standardized(pay.getMerchantCode().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getNote() != null && !pay.getNote().equals("")) {
					obj = cb.like(cb.lower(root.get("note")), "%" + CommonUtils.standardized(pay.getNote().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getStatus() == 0) {
					obj = cb.equal(cb.lower(root.get("status")),pay.getStatus());
					predicateList.add(obj);
				}
				if (pay.getStatus() == 1) {
					obj = cb.equal(cb.lower(root.get("status")),pay.getStatus());
					predicateList.add(obj);
				}
				query.orderBy(cb.desc(root.get("merchantsId")));

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}

		};
	}
}
