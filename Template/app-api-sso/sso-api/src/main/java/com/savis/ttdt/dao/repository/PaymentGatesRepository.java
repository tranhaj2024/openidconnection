package com.savis.ttdt.dao.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.savis.ttdt.dao.entity.PaymentGates;

@Repository
public interface PaymentGatesRepository extends JpaRepository<PaymentGates, Integer>, JpaSpecificationExecutor<PaymentGates> {
	
	List<PaymentGates> findByGateCode(String code);
	
	@Query("SELECT a FROM PaymentGates a WHERE a.gateCode in "
			+ "(SELECT b.gateCode FROM PaymentGates b WHERE b.gateCode != :codeOld) and a.gateCode like :codeNew\r\n" + 
			"")
	List<PaymentGates> SameCodes(@Param("codeOld") String codeOld, @Param("codeNew") String codeNew);
}
