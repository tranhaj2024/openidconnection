package com.savis.ttdt.endpoint.rest;


import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.endpoint.dto.common.JsonHelper;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentReceiptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/savis/lgsp/")
public class PaymentReceiptResource {

    @Autowired
    PaymentReceiptService paymentReceiptService;

    @RequestMapping(value = "/paymentReceipt", method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseData<Page<Object>> getAuthenticatedInfo( @RequestParam String search, Pageable pageable) {
        ResponseData<Page<Object>> response = new ResponseData<>();
        SearchObject searchObject = JsonHelper.jsonToObject(search, SearchObject.class);
        Page<Object> pay = null;

        try {
            pay = paymentReceiptService.getPaymentReceipt(searchObject, pageable);
            response.setData(pay);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);
        } catch (Exception e) {
            // TODO: handle exception
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

}
