package com.savis.ttdt.endpoint.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentGatesService;

@RestController
@RequestMapping("/api/v1/savis/lgsp/")
public class PaymentGatesResource {

	@Autowired
	private PaymentGatesService paymentGatesService;
	
	@GetMapping(value="payGates", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentGates>> getPage(Pageable pageable){
		ResponseData<Page<PaymentGates>> response = new ResponseData<>();
		
		Page<PaymentGates> pay = null;
		
		try {
			pay = paymentGatesService.getPage(pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@GetMapping(value="payGates/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Optional<PaymentGates>> getOne(@PathVariable("id") int id){
		ResponseData<Optional<PaymentGates>> response = new ResponseData<>();
		
		Optional<PaymentGates> pay = null;
		
		try {
			pay = paymentGatesService.findOne(id);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@PostMapping(value = "/payGates", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> create(@RequestBody PaymentGates paymentGates) {
		ResponseData<Integer> response = new ResponseData<>();

		int apps = -1;

		try {
			apps = paymentGatesService.create(paymentGates);
			if(apps == Constants.SUCCESS_CODE_FIELD_UNEXIST) {
				response.setData(Constants.SUCCESS_CODE_FIELD_UNEXIST);
			} else {
				throw new Exception();
			}

			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}

		return response;
	}
	
	@RequestMapping(value = "/payGates/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> deleteMultiple(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		
//		List<PaymentGates> apps = new ArrayList<PaymentGates>();
//		for (int i = 0; i < entityIds.length; i++) {
//			apps.add(paymentGatesService.findOne(entityIds[i]));
//		}
		
		int result = paymentGatesService.delete(id);
		if (result == 1) {
			response.setData(result);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} else {
			response.setData(0);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@PutMapping(value = "/payGates", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Integer> update(@RequestBody PaymentGates paymentGates) {
		ResponseData<Integer> response = new ResponseData<>();

		int apps = -1;

		try {
			apps = paymentGatesService.update(paymentGates);
			if(apps == Constants.SUCCESS_CODE_FIELD_UNEXIST1) {
				response.setData(Constants.SUCCESS_CODE_FIELD_UNEXIST1);
			} else {
				throw new Exception();
			}

			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}

		return response;
	}
	
	@PostMapping(value="payGates/search", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentGates>> search(@RequestBody PaymentGates paymentGates, Pageable pageable){
		ResponseData<Page<PaymentGates>> response = new ResponseData<>();
		
		Page<PaymentGates> pay = null;
		
		try {
			pay = paymentGatesService.search(paymentGates, pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
}
