package com.savis.ttdt.service.impl;

import com.savis.ttdt.dao.entity.Company;
import com.savis.ttdt.dao.repository.CompanyRepository;
import com.savis.ttdt.dao.repository.EmployeeRepository;
import com.savis.ttdt.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Optional<Company> getCompany ( Integer idC) {
        return companyRepository.findById(idC);
    }

    @Override
    public List<Company> getCompanys () {
        return companyRepository.findAll();
    }

    @Override
    public Integer countAllByIdc ( Integer idc ) {
        return companyRepository.countAllByIdc(idc);
    }


}
