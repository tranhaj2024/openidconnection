package com.savis.ttdt.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.repository.PaymentGatesRepository;
import com.savis.ttdt.dao.specifications.PaymentGatesSpecifications;
import com.savis.ttdt.service.PaymentGatesService;

@Service
public class PaymentGatesServiceImpl implements PaymentGatesService {
	
	@Autowired
	private PaymentGatesRepository paymentGatesRepository;
	
	private Logger LOGGER = LoggerFactory.getLogger(PaymentGatesServiceImpl.class);

	@Override
	public Page<PaymentGates> getPage(Pageable pageable) {
		// TODO Auto-generated method stub
		Page<PaymentGates> pay = null;
		try {
			pay = paymentGatesRepository.findAll(pageable);
			return pay;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			
		}
		return null;
	}

	@Override
	public int create(PaymentGates paymentGates) {
		// TODO Auto-generated method stub
		try {
			PaymentGates payCheck = findByGateCode(paymentGates.getGateCode());
			if(isSameCode("null", CommonUtils.standardizedCode(paymentGates.getGateCode()))) {
				System.out.println("2");
				return 2;
			}
			paymentGates.setStatus(1);
				paymentGatesRepository.save(paymentGates);
				System.out.println("3");
				return 3;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}
	
	@Override
	public PaymentGates findByGateCode(String code) {
		List<PaymentGates> provinces = null;
		try {
			provinces = paymentGatesRepository.findByGateCode(code);
			if(provinces != null) {
				if(provinces.size() == 1) {
					return provinces.get(0);
				} else if (provinces.size() > 1) {
					System.out.println("AgencyClone findByAgencyCode item > 1 !!!!!!!!!!!!!!!");
				}
			}
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public int update(PaymentGates paymentGates) {
		// TODO Auto-generated method stub
		try {
			paymentGatesRepository.save(paymentGates);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			paymentGatesRepository.deleteById(id);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int deleteAllBatch(Iterable<PaymentGates> paymentGates) {
		// TODO Auto-generated method stub
		try {
			paymentGatesRepository.deleteInBatch(paymentGates);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}

	@Override
	public Optional<PaymentGates> findOne(int id) {
		// TODO Auto-generated method stub
		Optional<PaymentGates> pay = null;
		try {
			pay = paymentGatesRepository.findById(id);
			return pay;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isSameCode(String codeOld, String codeNew) {
		List<PaymentGates> provinces = null;
		
		try {
			provinces = paymentGatesRepository.SameCodes(codeOld, codeNew);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		if(provinces != null && provinces.size() > 0) {
			return true;
		} return false;
	}

	@Override
	public Page<PaymentGates> search(PaymentGates paymentGates, Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			return paymentGatesRepository.findAll(PaymentGatesSpecifications.advanceFilter(paymentGates), pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

}
