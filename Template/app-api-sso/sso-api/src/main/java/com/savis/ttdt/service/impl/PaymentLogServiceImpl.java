package com.savis.ttdt.service.impl;

import java.util.*;

import com.savis.ttdt.common.dto.SearchForm;
import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.common.util.DateUtils;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import com.savis.ttdt.dao.specifications.PaymentGatesMerchantsLogSpecifications;
import com.savis.ttdt.dao.specifications.PaymentLogSpecifications;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import com.savis.ttdt.dao.entity.PaymentLog;
import com.savis.ttdt.dao.entity.ref.PaymentLogDTO;
import com.savis.ttdt.dao.repository.PaymentLogRepository;
import com.savis.ttdt.service.PaymentLogService;
import org.springframework.web.client.RestTemplate;

@Service
public class PaymentLogServiceImpl implements PaymentLogService {

	@Value("${api.elastic}")
	private String elasticURL;

	@Autowired
	private PaymentLogRepository payRepository;

	private Logger LOGGER = LoggerFactory.getLogger(PaymentLogServiceImpl.class);

	@Override
	public Page<PaymentLog> getPage(Pageable pageable) {
		// TODO Auto-generated method stub
		Page<PaymentLog> pay = null;
		try {
			pay = payRepository.findAll(pageable);
			return pay;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Optional<PaymentLog> findOne(int id) {
		// TODO Auto-generated method stub
		try {
			return payRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

//	convert string to json
	public Page<PaymentLogDTO> convertStringToObject(Pageable pageable) {
		List<PaymentLog> pay = null;
		try {
			pay = payRepository.findAll();
			List<PaymentLogDTO> payList = new ArrayList<PaymentLogDTO>();
			for (PaymentLog paymentLog : pay) {
				PaymentLogDTO payDTO = new PaymentLogDTO();
				JSONObject obj = new JSONObject(paymentLog.getResponseMessage());
				payDTO.setId(paymentLog.getId());
				try {
					payDTO.setMaLoi(obj.getString("MaLoi"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setMaXacThuc(obj.getString("MaXacThuc"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setMoTaLoi(obj.getString("MoTaLoi"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setUrlThanhToan(obj.getString("UrlThanhToan"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setBinary(obj.getString("binary"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setNoiDungHoaDon(obj.getString("NoiDungHoaDon"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setUrlBienLai(obj.getString("UrlBienLai"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				payList.add(payDTO);

			}
			Long start = pageable.getOffset();
			int start1 = start.intValue();
			Long end = (start + pageable.getPageSize()) > payList.size() ? payList.size()
					: (start + pageable.getPageSize());
			int end1 = end.intValue();
			Page<PaymentLogDTO> pages = new PageImpl<PaymentLogDTO>(payList.subList(start1, end1), pageable, payList.size());
			return pages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	findone
	@Override
	public PaymentLogDTO findOneDTO(int id) {
		List<PaymentLog> pay = null;
		try {
			pay = payRepository.findAll();
			List<PaymentLogDTO> payList = new ArrayList<PaymentLogDTO>();
			for (PaymentLog paymentLog : pay) {
				PaymentLogDTO payDTO = new PaymentLogDTO();
				JSONObject obj = new JSONObject(paymentLog.getResponseMessage());
				payDTO.setId(paymentLog.getId());
				try {
					payDTO.setMaLoi(obj.getString("MaLoi"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setMaXacThuc(obj.getString("MaXacThuc"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setMoTaLoi(obj.getString("MoTaLoi"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setUrlThanhToan(obj.getString("UrlThanhToan"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setBinary(obj.getString("binary"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setNoiDungHoaDon(obj.getString("NoiDungHoaDon"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					payDTO.setUrlBienLai(obj.getString("UrlBienLai"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				payList.add(payDTO);
			}
			for (PaymentLogDTO paymentLogDTO : payList) {
				if (paymentLogDTO.getId() == id) {
					return paymentLogDTO;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Page<Object> getLogLogin ( SearchForm searchForm, Pageable pageable ) {
		List<Object> pay = null;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Object> response = null;

		String esLink = elasticURL;

		boolean iscomman = false;
		String url = esLink + "/logstash-*/_search";
		String jsonSearch = "{\n" +
				"  \"query\": {\n" +
				"    \"bool\": {\n" +
				"      \"must\": [";

		if (StringUtils.isNotBlank(searchForm.getUserLogin())) {
			if(iscomman) jsonSearch += ",";
			iscomman = true;
			jsonSearch += "            {\n" +
					"               \"wildcard\":{\n" +
					"                  \"user_login\":\"" + searchForm.getUserLogin() + "\"\n" +
					"               }\n" +
					"            }\n";
		}

		if (StringUtils.isNotBlank(searchForm.getType_log())) {
			if(searchForm.getType_log().equals("in")) {
				if (iscomman) jsonSearch += ",";
				iscomman = true;
				jsonSearch += "            {\n" +
						"               \"wildcard\":{\n" +
						"                  \"action\":\"" + searchForm.getType_log() + "\"\n" +
						"               }\n" +
						"            }\n";
			} else {
				String[] listType = searchForm.getType_log().split(",");

				if (iscomman) jsonSearch += ",";
				iscomman = true;
				jsonSearch += "            {\n" +
						"               \"wildcard\":{\n" +
						"                  \"logout_number\":\"" + listType[0] + "\"\n" +
						"               }\n" +
						"            }\n" +
						"            ,{\n" +
						"               \"wildcard\":{\n" +
						"                  \"action\":\"" + listType[1] + "\"\n" +
						"               }\n" +
						"            }";
			}
		} else {
			if (iscomman) jsonSearch += ",";
			iscomman = true;

			jsonSearch += "            {\n" +
					"                \"regexp\": {\n" +
					"                    \"action\": \"(out|in)\"\n" +
					"                }\n" +
					"            }";
		}

		jsonSearch += "      ]\n";

		//filter date range
		jsonSearch += "," +
				"         \"filter\":[\n";
		if(searchForm.getFormDate() != null || searchForm.getToDate() != null ){
			jsonSearch += "            {\n" +
					"               \"range\":{\n" +
					"                  \"login_date\":{\n";
			if(searchForm.getFormDate() != null) {
				jsonSearch += "                     \"gte\":\"" + DateUtils.date2str(searchForm.getFormDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
			}
			if(searchForm.getToDate() != null) {
				jsonSearch += "                     \"lte\":\"" + DateUtils.date2str(searchForm.getToDate(), "dd-MM-yyyy HH:mm:ss") + "\",\n";
			}
			jsonSearch += "                     \"format\":\"dd-MM-yyyy HH:mm:ss\"\n" +
					"                  }\n" +
					"               }\n" +
					"            }\n";
		}
		jsonSearch += "         ]\n";

		//phân trang + sort
		String jsonCount = "    }\n" +
				"  },\n" +
				"  \"from\": " + pageable.getOffset() + ",\n" +
				"  \"size\": " + pageable.getPageSize() + ",\n" +
				"    \"sort\": [\n" +
				"        { \"login_date\": { \"order\": \"desc\" }}\n" +
				"    ]\n";
		String jsonEnd = "}";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<String> request = new HttpEntity<>(jsonSearch + jsonCount + jsonEnd, headers);
		response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);

		LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();
		LinkedHashMap<String, Object> mapHits = (LinkedHashMap<String, Object>) map.get("hits");
		List<Object> mapHit = (List<Object>) mapHits.get("hits");

		url = esLink + "/logstash-*/_count";
		request = new HttpEntity<>(jsonSearch + jsonEnd + jsonEnd + jsonEnd, headers);
		response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
		map = (LinkedHashMap<String, Object>) response.getBody();
		String count = map.get("count").toString();

		return new PageImpl<>(mapHit, pageable, Long.parseLong(count));
	}

	@Override
	public Page<PaymentLog> getPagePaymentLog ( SearchObject searchObject, Pageable pageable ) {
		try {
			Page<PaymentLog> paymentGatesMerchantsLogList = payRepository.findAll(PaymentLogSpecifications.advanceFilter(searchObject), pageable);
			return paymentGatesMerchantsLogList;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());

		}
		return null;
	}

}
