package com.savis.ttdt.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
	
	public static Date stringToDate(String dateStr){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return date;
	}

	public static Date stringToOnlyDate(String dateStr){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return date;
	}

	//Convert String to Date
	public static Date str2date(String input, String format) throws java.text.ParseException {
		Date result = null;
		if (!input.isEmpty()) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(format);
				result = formatter.parse(input);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	//Convert Date to String
	public static String date2str(Date input, String oFormat) {
		String result = "";
		if (input != null) {
			try {
				DateFormat df = new SimpleDateFormat(oFormat);
				result = df.format(input);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

}
