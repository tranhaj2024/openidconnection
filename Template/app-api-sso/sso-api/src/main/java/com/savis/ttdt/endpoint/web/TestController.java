package com.savis.ttdt.endpoint.web;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

	@GetMapping("/")
	public String test(Model model) {
		model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getName());
		return "test";

	}

}
