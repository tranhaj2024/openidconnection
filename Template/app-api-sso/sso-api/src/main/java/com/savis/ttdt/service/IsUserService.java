package com.savis.ttdt.service;

import com.savis.ttdt.endpoint.dto.user.AccessTokenInfo;
import com.savis.ttdt.endpoint.dto.user.UserInfo;

public interface IsUserService {

	UserInfo getUserInfo ( String authorizationCode );

	AccessTokenInfo getAccessToken ( String refreshToken );

}
