package com.savis.ttdt.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentMerchants;

public interface PaymentMerchantsService {
	Page<PaymentMerchants> getPage(Pageable pageable);
	int create(PaymentMerchants paymentMerchants);
	int update(PaymentMerchants paymentMerchants);
	int delete(int id);
	int deleteAllBatch(Iterable<PaymentMerchants> paymentMerchants);
	Optional<PaymentMerchants> findOne(int id);
	PaymentMerchants findByMerchantCode(String code);
	Page<PaymentMerchants> search(PaymentMerchants paymentMerchants, Pageable pageable);
}
