package com.savis.ttdt.common.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentReceipt {
    private String billCode;
    private String billName;
    private String timepPay;
}
