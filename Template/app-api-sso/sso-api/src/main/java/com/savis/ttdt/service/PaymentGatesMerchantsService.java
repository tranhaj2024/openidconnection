package com.savis.ttdt.service;

import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface PaymentGatesMerchantsService {
    Page<PaymentGatesMerchants> getPage( Pageable pageable);
    int create(PaymentGatesMerchants paymentGatesMerchants);
    int update(PaymentGatesMerchants paymentGatesMerchants);
    int delete(int id);
    Optional<PaymentGatesMerchants> findOne( int id);
    Page<PaymentGatesMerchants> search(PaymentGatesMerchants paymentGatesMerchants, Pageable pageable);
}
