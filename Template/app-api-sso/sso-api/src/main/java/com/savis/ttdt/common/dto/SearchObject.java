package com.savis.ttdt.common.dto;

import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchObject {
    private String orderid;
    private String ipAddress;
    private PaymentGatesMerchants dealid;
    private Date fromDate;
    private Date toDate;
    private int request_type;
    private String tradingCode;
    private String billCode;
    private String billName;

}
