package com.savis.ttdt.dao.specifications;

import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PaymentGatesMerchantsLogSpecifications {

    public static Specification<PaymentGatesMerchantsLog> advanceFilter( SearchObject pay) {
        return new Specification<PaymentGatesMerchantsLog>() {

            @Override
            public Predicate toPredicate( Root<PaymentGatesMerchantsLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;

                if(pay == null) {
                    return null;
                }

                obj = cb.notEqual(cb.lower(root.get("status")), 2 );
                predicateList.add(obj);
                if (pay.getDealid().getId() != 0 ) {
                    obj = cb.equal(cb.lower(root.get("paymentGateMerchant").get("id")), pay.getDealid().getId() );
                    predicateList.add(obj);
                }
                if (pay.getDealid().getGate().getGateCode()!= null && !pay.getDealid().getGate().getGateCode().equals("")) {
                    obj = cb.like(cb.lower(root.get("paymentGateMerchant").get("gate").get("gateCode")), "%" + CommonUtils.standardized(pay.getDealid().getGate().getGateCode()) + "%");
                    predicateList.add(obj);
                }
                if (pay.getOrderid() != null && !pay.getOrderid().equals("")) {
                    obj = cb.equal(root.get("orderId"),  pay.getOrderid() );
                    predicateList.add(obj);
                }
                if (pay.getIpAddress() != null && !pay.getIpAddress().equals("")) {
                    obj = cb.equal(cb.lower(root.get("ipAddress")),pay.getIpAddress());
                    predicateList.add(obj);
                }
                if (pay.getTradingCode() != null && !pay.getTradingCode().equals("")) {
                    obj = cb.equal(cb.lower(root.get("tradingCode")),pay.getTradingCode());
                    predicateList.add(obj);
                }
                if (pay.getFromDate() != null) {
                    obj = cb.greaterThanOrEqualTo(root.get("paymenttime"), pay.getFromDate());
                    predicateList.add(obj);
                }
                if (pay.getToDate() != null) {
                    obj = cb.lessThanOrEqualTo(root.get("paymenttime"), pay.getToDate());
                    predicateList.add(obj);
                }
                query.orderBy(cb.desc(root.get("id")));

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }

        };
    }
}
