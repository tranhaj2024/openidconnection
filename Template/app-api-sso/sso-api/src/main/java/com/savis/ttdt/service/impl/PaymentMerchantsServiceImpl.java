package com.savis.ttdt.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentMerchants;
import com.savis.ttdt.dao.repository.PaymentMerchantsRepository;
import com.savis.ttdt.dao.specifications.PaymentMerchantSpecifications;
import com.savis.ttdt.service.PaymentMerchantsService;

@Service
public class PaymentMerchantsServiceImpl implements PaymentMerchantsService {
	
	@Autowired
	private PaymentMerchantsRepository payReponse;
	
	private Logger LOGGER = LoggerFactory.getLogger(PaymentMerchantsServiceImpl.class);

	@Override
	public Page<PaymentMerchants> getPage(Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			return payReponse.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	@Override
	public int create(PaymentMerchants paymentMerchants) {
		// TODO Auto-generated method stub
		try {
			PaymentMerchants payCheck = findByMerchantCode(paymentMerchants.getMerchantCode());
			if(isSameCode("null", CommonUtils.standardizedCode(paymentMerchants.getMerchantCode()))) {
				System.out.println("2");
				return 2;
			}
			payReponse.save(paymentMerchants);
				System.out.println("3");
				return 3;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}
	
	@Override
	public PaymentMerchants findByMerchantCode(String code) {
		List<PaymentMerchants> provinces = null;
		try {
			provinces = payReponse.findByMerchantCode(code);
			if(provinces != null) {
				if(provinces.size() == 1) {
					return provinces.get(0);
				} else if (provinces.size() > 1) {
					System.out.println("AgencyClone findByAgencyCode item > 1 !!!!!!!!!!!!!!!");
				}
			}
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public int update(PaymentMerchants paymentMerchants) {
		// TODO Auto-generated method stub
		try {
			paymentMerchants.setStatus(1);
			payReponse.save(paymentMerchants);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		try {
			payReponse.deleteById(id);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int deleteAllBatch(Iterable<PaymentMerchants> paymentMerchants) {
		// TODO Auto-generated method stub
		try {
			payReponse.deleteInBatch(paymentMerchants);
			return 1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return 0;
	}

	@Override
	public Optional<PaymentMerchants> findOne(int id) {
		// TODO Auto-generated method stub
		try {
			return payReponse.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isSameCode(String codeOld, String codeNew) {
		List<PaymentMerchants> provinces = null;
		
		try {
			provinces = payReponse.SameCodes(codeOld, codeNew);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		if(provinces != null && provinces.size() > 0) {
			return true;
		} return false;
	}

	@Override
	public Page<PaymentMerchants> search(PaymentMerchants paymentMerchants, Pageable pageable) {
		// TODO Auto-generated method stub
		try {
			return payReponse.findAll(PaymentMerchantSpecifications.advanceFilter(paymentMerchants), pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

}
