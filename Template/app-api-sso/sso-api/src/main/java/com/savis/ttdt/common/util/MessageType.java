package com.savis.ttdt.common.util;

public enum MessageType {
	SUCCESS, INFO, WARNING, ERROR
}
