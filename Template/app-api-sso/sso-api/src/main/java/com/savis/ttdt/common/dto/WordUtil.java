package com.savis.ttdt.common.dto;

import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.variable.TableVariable;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variable;
import pl.jsolve.templ4docx.variable.Variables;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WordUtil {
    public Docx getFileDoc ( SearchObject object, List<PaymentGatesMerchantsLog> paymentGatesMerchantsLogs, InputStream path ) throws Exception {
        Docx docx = new Docx(path);
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        TableVariable tableVariable = new TableVariable();


        Variables variables = new Variables();
        variables.addTextVariable(new TextVariable("${day}", "" + day));
        variables.addTextVariable(new TextVariable("${month}", "" + (month+1)));
        variables.addTextVariable(new TextVariable("${year}", "" + year));
        variables.addTextVariable(new TextVariable("${startDate}",object.getFromDate() == null ? " " : String.valueOf(object.getFromDate())));
        variables.addTextVariable(new TextVariable("${endDate}", object.getToDate() == null ? " " : String.valueOf(object.getToDate())));
        variables.addTextVariable(new TextVariable("${orderID}", object.getOrderid() == null ? " " : object.getOrderid()));
        variables.addTextVariable(new TextVariable("${portID}", object.getDealid().getGate().getGateCode() == null ? " " : object.getDealid().getGate().getGateCode()));
        variables.addTextVariable(new TextVariable("${tradCode}", object.getTradingCode() == null ? " " :(object.getTradingCode().equals("")? " ":object.getTradingCode())));
        variables.addTextVariable(new TextVariable("${ipAddress}", object.getIpAddress() == null ? " " : object.getIpAddress()));


        List<Variable> stt = new ArrayList<>();
        List<Variable> orderid = new ArrayList<>();
        List<Variable> gateID = new ArrayList<>();
        List<Variable> deal = new ArrayList<>();
        List<Variable> ippAdress = new ArrayList<>();
        List<Variable> thType = new ArrayList<>();
        List<Variable> unit = new ArrayList<>();
        List<Variable> money = new ArrayList<>();
        List<Variable> mID = new ArrayList<>();
        List<Variable> bank = new ArrayList<>();
        List<Variable> status = new ArrayList<>();
        List<Variable> content = new ArrayList<>();
        if (paymentGatesMerchantsLogs != null) {

            for (int i = 0; i < paymentGatesMerchantsLogs.size(); i++) {
                stt.add(new TextVariable("${stt}", String.valueOf(i+1)));
                orderid.add(new TextVariable("${order}",String.valueOf(paymentGatesMerchantsLogs.get(i).getOrderId())));
                gateID.add(new TextVariable("${port}", String.valueOf(paymentGatesMerchantsLogs.get(i).getPaymentGateMerchant().getGate().getGateCode())));
                deal.add(new TextVariable("${deal}", String.valueOf(paymentGatesMerchantsLogs.get(i).getTradingCode())));
                ippAdress.add(new TextVariable("${ipAdd}", paymentGatesMerchantsLogs.get(i).getIpAddress()));
                thType.add(new TextVariable("${tTH}", String.valueOf(paymentGatesMerchantsLogs.get(i).getServiceType())));
                unit.add(new TextVariable("${unit}", String.valueOf(paymentGatesMerchantsLogs.get(i).getPaymentGateMerchant().getMerchant().getMerchantCode())));
                money.add(new TextVariable("${money}", String.valueOf(paymentGatesMerchantsLogs.get(i).getMoney())));
                mID.add(new TextVariable("${mID}", String.valueOf(paymentGatesMerchantsLogs.get(i).getTypemoney())));
                bank.add(new TextVariable("${bank}", String.valueOf(paymentGatesMerchantsLogs.get(i).getBank())));
                status.add(new TextVariable("${status}",
                        String.valueOf(paymentGatesMerchantsLogs.get(i).getStatus()== 0 ? "Thất Bại" : "Thành Công")));
                content.add(new TextVariable("${content}", String.valueOf(paymentGatesMerchantsLogs.get(i).getNote())));
            }
            tableVariable.addVariable(stt);
            tableVariable.addVariable(orderid);
            tableVariable.addVariable(gateID);
            tableVariable.addVariable(deal);
            tableVariable.addVariable(ippAdress);
            tableVariable.addVariable(thType);
            tableVariable.addVariable(unit);
            tableVariable.addVariable(money);
            tableVariable.addVariable(mID);
            tableVariable.addVariable(bank);
            tableVariable.addVariable(status);
            tableVariable.addVariable(content);
            variables.addTableVariable(tableVariable);
        }
        docx.fillTemplate(variables);
        return docx;
    }
}
