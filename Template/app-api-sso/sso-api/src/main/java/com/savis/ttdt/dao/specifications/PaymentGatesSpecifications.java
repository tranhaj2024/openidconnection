package com.savis.ttdt.dao.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentGates;

public class PaymentGatesSpecifications {
	public static Specification<PaymentGates> advanceFilter(PaymentGates pay) {
		return new Specification<PaymentGates>() {

			@Override
			public Predicate toPredicate(Root<PaymentGates> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;
				
				if(pay == null) {
					return null;
				}
				if (pay.getGateName() != null && !pay.getGateName().equals("")) {
					obj = cb.like(cb.lower(root.get("gateName")), "%" + CommonUtils.standardized(pay.getGateName().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getGateCode() != null && !pay.getGateCode().equals("")) {
					obj = cb.like(cb.lower(root.get("gateCode")), "%" + CommonUtils.standardized(pay.getGateCode().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getNote() != null && !pay.getNote().equals("")) {
					obj = cb.like(cb.lower(root.get("note")), "%" + CommonUtils.standardized(pay.getNote().toLowerCase()) + "%");
					predicateList.add(obj);
				}
				if (pay.getStatus() == 0) {
					obj = cb.equal(cb.lower(root.get("status")),pay.getStatus());
					predicateList.add(obj);
				}
				if (pay.getStatus() == 1) {
					obj = cb.equal(cb.lower(root.get("status")),pay.getStatus());
					predicateList.add(obj);
				}
				query.orderBy(cb.desc(root.get("gateId")));

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}

		};
	}
}
