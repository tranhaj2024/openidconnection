package com.savis.ttdt.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "payment_log")
@Getter
@Setter
@Data
@AllArgsConstructor
public class PaymentLog {
	@Id
	@TableGenerator(name = "gen_id", table = "hibernate_gen_id", pkColumnName = "gen_name", valueColumnName = "gen_value", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gen_id")
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private int id;
	@Column(name = "request_message")
	private String requestMessage;
	@Column(name = "request_time")
	private Date requestTime;
	@Column(name = "response_message")
	private String responseMessage;
	@Column(name = "response_time")
	private String responseTime;
	@Column(name = "request_type")
	private String requestType;
	public PaymentLog() {
		super();
	}
	
}
