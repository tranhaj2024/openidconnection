package com.savis.ttdt.dao.repository;

import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface PaymentGatesMerchantsRepository extends JpaRepository<PaymentGatesMerchants, Integer> {
}
