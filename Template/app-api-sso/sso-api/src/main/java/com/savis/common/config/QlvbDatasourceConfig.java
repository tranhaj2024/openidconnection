package com.savis.common.config;

import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "qlvbEntityManagerFactory", transactionManagerRef = "qlvbTransactionManager", basePackages = "com.savis.qlvb.dao.repository")
public class QlvbDatasourceConfig {
	@Bean
	@Primary
	@ConfigurationProperties("app.qlvb.datasource")
	public DataSourceProperties qlvbDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "qlvbDataSource")
	@Primary
	public HikariDataSource qlvbDataSource() {
		return qlvbDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Primary
	@Bean(name = "qlvbEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean qlvbEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("commonJpaProperties") Map<String, String> jpaProperties) {
		return builder.dataSource(qlvbDataSource()).packages("com.savis.qlvb.dao.entity").persistenceUnit("qlvb-db")
				.properties(jpaProperties).build();
	}

	@Primary
	@Bean(name = "qlvbTransactionManager")
	public PlatformTransactionManager qlvbTransactionManager(
			@Qualifier("qlvbEntityManagerFactory") EntityManagerFactory qlvbEntityManagerFactory) {
		return new JpaTransactionManager(qlvbEntityManagerFactory);
	}
}