package com.savis.ttdt.dao.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Date;

@Entity
@Table(name = "payment_gates_merchants_log")
@Getter
@Setter
@Data
@AllArgsConstructor
public class PaymentGatesMerchantsLog {
	@Id
	@TableGenerator(name = "gen_id", table = "hibernate_gen_id", pkColumnName = "gen_name", valueColumnName = "gen_value", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "gen_id")
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private int id;
	@Column(name = "order_id")
	private String orderId;
	@ManyToOne
	@JoinColumn(name = "payment_gate_merchant_id")
	private PaymentGatesMerchants paymentGateMerchant;
	@Column(name = "service_type")
	private int serviceType;
	@Column(name = "note")
	private String note;
	@Column(name = "confirm_url")
	private String confirmUrl;
	@Column(name = "amount_money")
	private double money;
	@Column(name = "type_money")
	private String typemoney;
	@Column(name = "bank")
	private String bank;
	@Column(name = "status")
	private int status;
	@Column(name = "error")
	private String error;
	@Column(name = "payment_time")
	private Date paymenttime;
	@Column(name = "ip_address")
	private String ipAddress;
	@Column(name = "th_type")
	private String thType;
	@Column(name = "trading_code")
	private String tradingCode;
	public PaymentGatesMerchantsLog() {
		super();
	}
	
}
