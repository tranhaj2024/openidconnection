package com.savis.ttdt.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDataSearchDTO {
    private String startDate;
    /** end data of the search range */
    private String endDate;
    /** end data of the search date format */
    private String orderID;
    /** end data of the search time select */
    private String portID;

    /** ur ip id search */
    private String ipAddress;
    /** deal ID search */
    private String dealID;
}
