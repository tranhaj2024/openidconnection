package com.savis.ttdt.service;

import com.savis.ttdt.common.dto.SearchForm;
import com.savis.ttdt.common.dto.SearchObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentReceiptService {
    Page<Object> getPaymentReceipt( SearchObject search, Pageable pageable);
}
