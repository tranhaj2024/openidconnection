package com.savis.ttdt.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentMerchants;

public interface PaymentMerchantsRepository extends JpaRepository<PaymentMerchants, Integer>, JpaSpecificationExecutor<PaymentMerchants> {
	List<PaymentMerchants> findByMerchantCode(String code);

	@Query("SELECT a FROM PaymentMerchants a WHERE a.merchantCode in "
			+ "(SELECT b.merchantCode FROM PaymentMerchants b WHERE b.merchantCode != :codeOld) and a.merchantCode like :codeNew\r\n"
			+ "")
	List<PaymentMerchants> SameCodes(@Param("codeOld") String codeOld, @Param("codeNew") String codeNew);
}
