package com.savis.ttdt.service;

import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface PaymentGatesMerchantsLogService {
    List<PaymentGatesMerchantsLog> findAll();
    Page<PaymentGatesMerchantsLog> getPage( Pageable pageable);
    int create(PaymentGatesMerchantsLog paymentGatesMerchantsLog);
    int update(PaymentGatesMerchantsLog paymentGatesMerchantsLog);
    int delete(int id);
    Optional<PaymentGatesMerchantsLog> findOne( int id);
    Page<PaymentGatesMerchantsLog> searchPage( SearchObject searchObject, Pageable pageable);
    List<PaymentGatesMerchantsLog> searchAll(SearchObject searchObject);
}
