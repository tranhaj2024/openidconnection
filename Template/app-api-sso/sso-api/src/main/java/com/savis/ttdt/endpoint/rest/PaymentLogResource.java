package com.savis.ttdt.endpoint.rest;

import java.util.Optional;

import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import com.savis.ttdt.endpoint.dto.common.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.google.gson.JsonObject;
import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentLog;
import com.savis.ttdt.dao.entity.ref.PaymentLogDTO;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentLogService;

@RestController
@RequestMapping("/api/v1/savis/lgsp/")
public class PaymentLogResource {
	
	@Autowired
	private PaymentLogService payLogService;
	
	@GetMapping(value="payLogs/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Optional<PaymentLog>> getOne(@PathVariable("id") int id){
		ResponseData<Optional<PaymentLog>> response = new ResponseData<>();
		
		Optional<PaymentLog> pay = null;
		
		try {
			pay = payLogService.findOne(id);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@GetMapping(value="payLogs", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentLog>> getPage(Pageable pageable){
		ResponseData<Page<PaymentLog>> response = new ResponseData<>();
		
		Page<PaymentLog> pay = null;
		
		try {
			pay = payLogService.getPage(pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@GetMapping(value="payLogs/json", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<Page<PaymentLogDTO>> getJson(Pageable pageable){
		ResponseData<Page<PaymentLogDTO>> response = new ResponseData<>();
		
		Page<PaymentLogDTO> pay = null;
		
		try {
			pay = payLogService.convertStringToObject(pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
	
	@GetMapping(value="payLogs/json/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public @ResponseBody ResponseData<PaymentLogDTO> findOne(@PathVariable("id") int id){
		ResponseData<PaymentLogDTO> response = new ResponseData<>();
		
		PaymentLogDTO pay = null;
		
		try {
			pay = payLogService.findOneDTO(id);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO: handle exception
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}

	@RequestMapping(value = "list/payment-log", method = RequestMethod.GET)
	public @ResponseBody
	ResponseData<Page<PaymentLog>> searchLogin( @RequestParam(required = false) String search, Pageable pageable) {
		ResponseData<Page<PaymentLog>> response = new ResponseData<>();
		SearchObject searchObject = JsonHelper.jsonToObject(search, SearchObject.class);
		Page<PaymentLog> pay =null ;
		try {
			pay = payLogService.getPagePaymentLog(searchObject, pageable);
			response.setData(pay);
			response.setCode(Constants.SUCCESS_CODE);
			response.setMessage(Constants.SUCCESS_MSG);

		} catch (Exception e) {
			e.printStackTrace();
			response.setData(null);
			response.setCode(Constants.ERR_CODE_BAD_REQUEST);
			response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
		}
		return response;
	}
}
