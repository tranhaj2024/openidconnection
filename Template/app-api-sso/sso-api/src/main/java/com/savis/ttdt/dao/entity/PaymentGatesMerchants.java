package com.savis.ttdt.dao.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "payment_gates_merchants")
@Getter
@Setter
@Data
@AllArgsConstructor
public class PaymentGatesMerchants {
	@Id
	@TableGenerator(name = "gen_id", table = "hibernate_gen_id", pkColumnName = "gen_name", valueColumnName = "gen_value", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "gen_id")
	@Column(name = "Id", unique = true, nullable = false, precision = 22, scale = 0)
	private int id;
	@ManyToOne
	@JoinColumn(name = "GateId")
	private PaymentGates gate;
	@ManyToOne
	@JoinColumn(name = "MerchantId")
	private PaymentMerchants merchant;
	@Column(name = "AccessKey")
	private String accessKey;
	@Column(name = "SecretKey")
	private String secretKey;
	@Column(name = "PublicKey")
	private String publicKey;
	
	public PaymentGatesMerchants() {
		super();
	}
	
	
	
}
