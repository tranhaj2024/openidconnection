package com.savis.ttdt.common.dto;

import java.io.Serializable;
import java.util.Date;

public class SearchForm implements Serializable {

    private String type_log;
    private Date formDate;
    private Date toDate;
    private String userLogin;


    public String getUserLogin () {
        return userLogin;
    }

    public void setUserLogin ( String userLogin ) {
        this.userLogin = userLogin;
    }

    public Date getFormDate () {
        return formDate;
    }

    public void setFormDate ( Date formDate ) {
        this.formDate = formDate;
    }

    public Date getToDate () {
        return toDate;
    }

    public void setToDate ( Date toDate ) {
        this.toDate = toDate;
    }

    public String getType_log () {
        return type_log;
    }

    public void setType_log ( String type_log ) {
        this.type_log = type_log;
    }

}