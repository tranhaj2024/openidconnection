package com.savis.ttdt.service.impl;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentGates;
import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import com.savis.ttdt.dao.repository.PaymentGatesMerchantsRepository;
import com.savis.ttdt.service.PaymentGatesMerchantsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaymentGatesMechantsServiceImpl implements PaymentGatesMerchantsService {
    @Autowired
    PaymentGatesMerchantsRepository paymentGatesMerchantsRepository;

    private Logger LOGGER = LoggerFactory.getLogger(PaymentGatesMechantsServiceImpl.class);
    @Override
    public Page<PaymentGatesMerchants> getPage ( Pageable pageable ) {
        // TODO Auto-generated method stub
        Page<PaymentGatesMerchants> pay = null;
        try {
            pay = paymentGatesMerchantsRepository.findAll(pageable);
            return pay;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());

        }
        return null;
    }

    @Override
    public int create ( PaymentGatesMerchants paymentGatesMerchants ) {
        // TODO Auto-generated method stub
        try {
            paymentGatesMerchantsRepository.save(paymentGatesMerchants);
            return 1;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int update ( PaymentGatesMerchants paymentGatesMerchants ) {
        // TODO Auto-generated method stub
        try {
            paymentGatesMerchantsRepository.save(paymentGatesMerchants);
            return 1;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete ( int id ) {
        // TODO Auto-generated method stub
        try {
            paymentGatesMerchantsRepository.deleteById(id);
            return 1;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Optional<PaymentGatesMerchants> findOne ( int id ) {
        // TODO Auto-generated method stub
        Optional<PaymentGatesMerchants> pay = null;
        try {
            pay = paymentGatesMerchantsRepository.findById(id);
            return pay;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Page<PaymentGatesMerchants> search ( PaymentGatesMerchants paymentGatesMerchants, Pageable pageable ) {
        return null;
    }
}
