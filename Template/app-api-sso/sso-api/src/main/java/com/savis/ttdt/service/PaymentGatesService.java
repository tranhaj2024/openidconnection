package com.savis.ttdt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.savis.ttdt.dao.entity.PaymentGates;


public interface PaymentGatesService {

	Page<PaymentGates> getPage(Pageable pageable);
	int create(PaymentGates paymentGates);
	int update(PaymentGates paymentGates);
	int delete(int id);
	int deleteAllBatch(Iterable<PaymentGates> paymentGates);
	Optional<PaymentGates> findOne(int id);
	PaymentGates findByGateCode(String code);
	Page<PaymentGates> search(PaymentGates paymentGates, Pageable pageable);
}
