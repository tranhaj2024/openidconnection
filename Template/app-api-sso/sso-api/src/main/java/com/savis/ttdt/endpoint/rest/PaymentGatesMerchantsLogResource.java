package com.savis.ttdt.endpoint.rest;

import com.savis.ttdt.common.constants.Constants;
import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.common.dto.WordUtil;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import com.savis.ttdt.endpoint.dto.common.JsonHelper;
import com.savis.ttdt.endpoint.dto.common.ResponseData;
import com.savis.ttdt.service.PaymentGatesMerchantsLogService;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.jsolve.sweetener.criteria.restriction.In;
import pl.jsolve.templ4docx.core.Docx;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/savis/lgsp/")
public class PaymentGatesMerchantsLogResource {
    @Autowired
    private PaymentGatesMerchantsLogService paymentGatesMerchantsLogService;

    @GetMapping(value="payGatesMerchantsLog", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public @ResponseBody ResponseData<Page<PaymentGatesMerchantsLog>> getPage( Pageable pageable){
        ResponseData<Page<PaymentGatesMerchantsLog>> response = new ResponseData<>();

        Page<PaymentGatesMerchantsLog> pay = null;

        try {
            pay = paymentGatesMerchantsLogService.getPage(pageable);
            response.setData(pay);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);
        } catch (Exception e) {
            // TODO: handle exception
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/list/receptpaper", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<PaymentGatesMerchantsLog>> searchLogin( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<PaymentGatesMerchantsLog>> response = new ResponseData<>();
        SearchObject searchObject = JsonHelper.jsonToObject(search, SearchObject.class);
        Page<PaymentGatesMerchantsLog> pay =null ;
        try {
                pay = paymentGatesMerchantsLogService.searchPage(searchObject, pageable);
                response.setData((Page<PaymentGatesMerchantsLog>) pay);
                response.setCode(Constants.SUCCESS_CODE);
                response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/payment-gates-merchantslog", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Optional<PaymentGatesMerchantsLog>> searchLogin( @RequestParam(required = false) int id) {
        ResponseData<Optional<PaymentGatesMerchantsLog>> response = new ResponseData<>();
        Optional<PaymentGatesMerchantsLog> pay =null ;
        try {
            pay = paymentGatesMerchantsLogService.findOne(id);
            response.setData(pay);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @GetMapping("/list/receptpaper/export-file")
    public void exportWord( HttpServletResponse response, Pageable pageable,
                            @RequestParam(required = false) String search , @RequestParam(value = "option" ,required = false) Integer opptionDownload) {
        try {
            SearchObject searchObject = JsonHelper.jsonToObject(search, SearchObject.class);
            List<PaymentGatesMerchantsLog> pay = paymentGatesMerchantsLogService.searchAll(searchObject); ;
            SimpleDateFormat dateFormatHeader = new SimpleDateFormat("dd?MM/yyyy");
            DateFormat dateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");
            DateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
            response.setHeader("Content-Disposition", "attachment; filename= BienNhanThanhToan_" + dateFormatHeader.format(new Date()) + ".docx");
            ServletOutputStream out = response.getOutputStream();
            InputStream resource = new ClassPathResource("/file/templateAuction.docx").getInputStream();
            WordUtil wordUtil = new WordUtil();
            Docx docx = wordUtil.getFileDoc(searchObject, pay, resource);

            XWPFDocument documentOutPut = docx.getXWPFDocument();
            if (opptionDownload == 1) {
                PdfOptions options = PdfOptions.create();
                OutputStream outputStream = new FileOutputStream("WordDocument.pdf");
                PdfConverter.getInstance().convert(documentOutPut, outputStream, options);
                response.setHeader("Content-Disposition", "attachment; filename= BienNhanThanhToan_" + dateFormatHeader.format(new Date()) + ".pdf");
                InputStreamResource fileResource = new InputStreamResource(new FileInputStream("WordDocument.pdf"));
                InputStream is = fileResource.getInputStream();
                byte[] bytes = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(bytes)) != -1) {
                    // Ghi dữ liệu ảnh vào Response.
                    response.getOutputStream().write(bytes, 0, bytesRead);
                }
                is.close();
                out.flush();
                out.close();
            }
            documentOutPut.write(out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequestMapping(value = "/payment-gates-merchantslog", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> deletePGML( @RequestParam(required = false) int id) {
        Optional<PaymentGatesMerchantsLog> paymentGatesMerchantsLog = paymentGatesMerchantsLogService.findOne(id);
        paymentGatesMerchantsLog.get().setStatus(2);
        ResponseEntity<Integer> response = null;
        try {
            paymentGatesMerchantsLogService.update(paymentGatesMerchantsLog.get());
            response = new ResponseEntity<Integer>(1, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<Integer>(0, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/list/receptpaper", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> deleteAllPGML( @RequestParam(value = "ids",required = false) int[] entityIds) {
        ResponseEntity<Integer> response = null;
        try {
            for(int i = 0; i < entityIds.length; i++) {
                Optional<PaymentGatesMerchantsLog> paymentGatesMerchantsLog = paymentGatesMerchantsLogService.findOne(entityIds[i]);
                paymentGatesMerchantsLog.get().setStatus(2);
                paymentGatesMerchantsLogService.update(paymentGatesMerchantsLog.get());
            }
            response = new ResponseEntity<Integer>(1, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<Integer>(0, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
