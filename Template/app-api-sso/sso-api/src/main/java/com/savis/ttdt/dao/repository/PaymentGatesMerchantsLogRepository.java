package com.savis.ttdt.dao.repository;

import com.savis.ttdt.dao.entity.PaymentGatesMerchants;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface PaymentGatesMerchantsLogRepository extends JpaRepository<PaymentGatesMerchantsLog, Integer>, JpaSpecificationExecutor<PaymentGatesMerchantsLog> {
    public List<PaymentGatesMerchantsLog> findAllByOrderIdAndPaymentGateMerchantIdAndIpAddressAndPaymenttimeBetweenOrderByPaymenttime
            ( String oderid, PaymentGatesMerchants dealid, String ipAddress, DateTime startTime, DateTime endTime );
}
