package com.savis.ttdt.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Entity
@Table(name = "payment_merchants")
@Getter
@Setter
@Data
@AllArgsConstructor
public class PaymentMerchants {
	@Id
	@TableGenerator(name = "gen_id", table = "hibernate_gen_id", pkColumnName = "gen_name", valueColumnName = "gen_value", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gen_id")
	@Column(name = "MerchantId", unique = true, nullable = false, precision = 22, scale = 0)
	private int merchantsId;
	@Column(name = "MerchantCode")
	private String merchantCode;
	@Column(name = "MerchantName")
	private String merchantName;
	@Column(name = "Status")
	private int status;
	@Column(name = "Note")
	private String note;
	public PaymentMerchants() {
		super();
	}
	
	
}
