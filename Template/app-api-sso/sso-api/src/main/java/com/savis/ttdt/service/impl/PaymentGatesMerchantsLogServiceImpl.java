package com.savis.ttdt.service.impl;

import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.dao.entity.PaymentGatesMerchantsLog;
import com.savis.ttdt.dao.repository.PaymentGatesMerchantsLogRepository;
import com.savis.ttdt.dao.specifications.PaymentGatesMerchantsLogSpecifications;
import com.savis.ttdt.service.PaymentGatesMerchantsLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentGatesMerchantsLogServiceImpl implements PaymentGatesMerchantsLogService {
    @Autowired
    PaymentGatesMerchantsLogRepository paymentGatesMerchantsLogRepository;

    private Logger LOGGER = LoggerFactory.getLogger(PaymentGatesMerchantsLogServiceImpl.class);

    @Override
    public List<PaymentGatesMerchantsLog> findAll () {
        return paymentGatesMerchantsLogRepository.findAll();
    }

    @Override
    public Page<PaymentGatesMerchantsLog> getPage ( Pageable pageable ) {
        // TODO Auto-generated method stub
        Page<PaymentGatesMerchantsLog> pay = null;
        try {
            pay = paymentGatesMerchantsLogRepository.findAll(pageable);
            return pay;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());

        }
        return null;
    }

    @Override
    public int create ( PaymentGatesMerchantsLog paymentGatesMerchantsLog ) {
        return 0;
    }

    @Override
    public int update ( PaymentGatesMerchantsLog paymentGatesMerchantsLog ) {
        try {
            paymentGatesMerchantsLogRepository.save(paymentGatesMerchantsLog);
        }catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return 0;
        }
        return 1;
    }

    @Override
    public int delete ( int id ) {
        return 0;
    }

    @Override
    public Optional<PaymentGatesMerchantsLog> findOne ( int id ) {
        return paymentGatesMerchantsLogRepository.findById(id);
    }

    @Override
    public Page<PaymentGatesMerchantsLog> searchPage ( SearchObject searchObject, Pageable pageable ) {
        // TODO Auto-generated method stub
        try {
            Page<PaymentGatesMerchantsLog> paymentGatesMerchantsLogList = paymentGatesMerchantsLogRepository.findAll(PaymentGatesMerchantsLogSpecifications.advanceFilter(searchObject), pageable);
            return paymentGatesMerchantsLogList;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());

        }
        return null;
    }

    @Override
    public List<PaymentGatesMerchantsLog> searchAll ( SearchObject searchObject ) {
        // TODO Auto-generated method stub
        try {
            List<PaymentGatesMerchantsLog> paymentGatesMerchantsLogList = paymentGatesMerchantsLogRepository.findAll(PaymentGatesMerchantsLogSpecifications.advanceFilter(searchObject));
            return paymentGatesMerchantsLogList;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            LOGGER.error(e.getMessage());

        }
        return null;
    }
}
