package com.savis.ttdt.dao.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.savis.ttdt.common.dto.SearchObject;
import org.springframework.data.jpa.domain.Specification;

import com.savis.ttdt.common.util.CommonUtils;
import com.savis.ttdt.dao.entity.PaymentLog;
import com.savis.ttdt.dao.entity.PaymentMerchants;

public class PaymentLogSpecifications {
	public static Specification<PaymentLog> advanceFilter( SearchObject pay) {
		return new Specification<PaymentLog>() {

			@Override
			public Predicate toPredicate(Root<PaymentLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<>();
				Predicate obj = null;

				if(pay == null) {
					return null;
				}
				if (pay.getRequest_type() != 0) {
					obj = cb.equal(cb.lower(root.get("requestType")),pay.getRequest_type());
					predicateList.add(obj);
				}
				if (pay.getFromDate() != null) {
					obj = cb.greaterThanOrEqualTo(root.get("requestTime"), pay.getFromDate());
					predicateList.add(obj);
				}
				if (pay.getToDate() != null) {
					obj = cb.lessThanOrEqualTo(root.get("requestTime"), pay.getToDate());
					predicateList.add(obj);
				}

				query.orderBy(cb.desc(root.get("id")));

				return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}

		};
	}
}
