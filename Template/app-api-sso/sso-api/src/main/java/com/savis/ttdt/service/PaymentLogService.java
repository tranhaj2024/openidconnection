package com.savis.ttdt.service;



import java.util.List;
import java.util.Optional;

import com.savis.ttdt.common.dto.SearchForm;
import com.savis.ttdt.common.dto.SearchObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.google.gson.JsonObject;
import com.savis.ttdt.dao.entity.PaymentLog;
import com.savis.ttdt.dao.entity.ref.PaymentLogDTO;

public interface PaymentLogService {
	Page<PaymentLog> getPage(Pageable pageable);
	Optional<PaymentLog> findOne(int id);
	Page<PaymentLogDTO> convertStringToObject(Pageable pageable);
	PaymentLogDTO findOneDTO(int id);
//	List<PaymentLogDTO> getListPayLogDTO();
	Page<Object> getLogLogin( SearchForm searchForm, Pageable pageable);
	Page<PaymentLog> getPagePaymentLog( SearchObject searchForm, Pageable pageable);
}
