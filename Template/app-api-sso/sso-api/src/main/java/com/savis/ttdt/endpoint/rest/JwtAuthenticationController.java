package com.savis.ttdt.endpoint.rest;

import com.savis.common.config.JBcryptHelper;
import com.savis.ttdt.dao.entity.Employee;
import com.savis.ttdt.dao.entity.JwtRequest;
import com.savis.ttdt.dao.entity.JwtResponse;
import com.savis.ttdt.service.EmployeeService;
import com.savis.ttdt.service.impl.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JBcryptHelper jBcryptHelper;


    @PostMapping(value = "/login")
    public ResponseEntity<?> createAuthenticationToken( @RequestBody JwtRequest authenticationRequest) throws Exception {
        try {
            //authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
            String email = authenticationRequest.getEmail();
            String planTextPassword = authenticationRequest.getPassword();

            //Lấy employee từ email
            Employee employee = employeeService.loadEmployeeFromEmail(email);
            //Công chuỗi plantext và hash
            String hashPassword = planTextPassword + employee.getPassword_salt();
            if (jBcryptHelper.checkBcryptPassword(hashPassword, employee.getPassworde())) {
                final String token = jwtTokenUtil.generateToken(employee);
                return ResponseEntity.ok(new JwtResponse(token));
            }
            return new ResponseEntity<>(true, HttpStatus.NOT_FOUND);
        }catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Error in createAuthenticationToken Controller");
            return new ResponseEntity<>(true, HttpStatus.CONFLICT);
        }
    }
}
