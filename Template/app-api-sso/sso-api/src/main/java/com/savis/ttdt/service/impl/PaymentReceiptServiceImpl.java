package com.savis.ttdt.service.impl;

import com.savis.ttdt.common.dto.SearchForm;
import com.savis.ttdt.common.dto.SearchObject;
import com.savis.ttdt.service.PaymentReceiptService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class PaymentReceiptServiceImpl implements PaymentReceiptService {

    @Value("${api.payment_receipt}")
    private String paymentReceiptURL;

    @Override
    public Page<Object> getPaymentReceipt ( SearchObject search, Pageable pageable ) {
        List<Object> paymentReceipt = null;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> response = null;

        String url = paymentReceiptURL+"?search="+search+"&"+pageable;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));


        HttpEntity<String> request = new HttpEntity<>(headers);
        response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);

        List<Object> listPaymentReceipt = (List<Object>) response.getBody();
        LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) response.getBody();
        String count = map.get("count").toString();
        return new PageImpl<>(listPaymentReceipt, pageable, Long.parseLong(count));
    }
}
