import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from './user';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from './employee.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validForm:  FormGroup;
  user : User = new User();
  location : Location;

  constructor(private route: ActivatedRoute,private router: Router,
              private employeeService: EmployeeService, private fb : FormBuilder) {
  }

  login() {
    if(localStorage.getItem("token") == null) {
      this.employeeService.login(this.user).subscribe(data => {
        console.log(data);
        if (data) {
          this.router.navigate(['/home']);
          localStorage.setItem("token", data.token);
        }

      }),
        error => {
          alert("Lỗi hệ thống ");
          this.router.navigate(['/login']);
          console.log(error);
        };
      document.getElementById("miss").innerHTML = "Sai tài khoản hoặc mật khẩu";
      this.reload();
    } else {
      this.router.navigate(['/home']);
    }
  }


  reload() {
    this.router.navigate(['/login']);
  }

  onSubmit() {
    this.login();
  }

  ngOnInit(): void {
    if(localStorage.getItem("token") != null) {
      alert("Bạn đã đăng nhập rồi");
      this.router.navigate(['/home']);
    }

    this.validForm = new FormGroup({
      'email' : new FormControl(this.user.email, [
        Validators.required,
        Validators.email,
      ]),
      'password' : new FormControl(this.user.password, [
        Validators.minLength(6),
      ])
    });
  }

  get email() {
    return this.validForm.get('email');
  }

  get password() {
    return this.validForm.get('password');
  }

}
