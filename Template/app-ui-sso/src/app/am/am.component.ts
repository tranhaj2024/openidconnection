import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageItem, LanguageItemList } from '../i18n-setting';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { UserInfo } from './common/shared/user-info/user-info';
import { TokenInfo } from './common/shared/user-info/token-info';
@Component({
  selector: 'app-am',
  templateUrl: './am.component.html',
  styleUrls: ['./am.component.css'],
  providers: [Idle, TranslateService]
})
export class AmComponent implements OnInit {

  userInfo: UserInfo;
  tokenInfo: TokenInfo;

  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  private expireTime: number = 0;
  private timer;
  private sub;

  ListLanguage: LanguageItem[];
  SelectedLanguage: LanguageItem;
  constructor(
    private http: Http,
    private idle: Idle,
    private router: Router,
    private translate: TranslateService,
  ) {
    this.ListLanguage = LanguageItemList;
    // Get current language
    this.ListLanguage.forEach(lang => {
      if (lang.Key === translate.currentLang) {
        this.SelectedLanguage = lang;
      }
    });

    idle.setIdle(5);
    idle.setTimeout(10);
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');


    idle.onTimeout.subscribe(() => {
      alert('Timeout');
      this.idleState = 'Timed out!';
      this.timedOut = true;
      localStorage.clear();
      this.router.navigate(['/auth', { sessionExpirate: 'true' }]);
    });
  }

  ngOnInit() {
    this.userInfo = JSON.parse('{'
    +'"userName":"admin",'
    +'"loweredUsername":"admin",'
    +'"mobileAlias":"0988888888",'
    +'"isAnonymous":0,"userType":0,'
    +'"applicationId":0,'
    +'"accessTokenInfo":{'
      +'"accessToken":"688210ac-c271-38e9-ba62-19f474d5b315",'
      +'"refreshToken":"5411a4d6-3d2d-32d6-8113-08473c1fed04",'
      +'"expiresIn":3600,'
      +'"idToken":"eyJ4NXQiOiJOVEF4Wm1NeE5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJraWQiOiJOVEF4Wm1NeE5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiVHg2Qk54TG5PN01hTUdZZ0xtMDhLZyIsImF1ZCI6IkdRdml6S01ZMU5ZOG9TNmc4ek5QQjdraGp5c2EiLCJjX2hhc2giOiJiYUo0Zm11TlRVa3lYcEsyRVpvVWt3Iiwic3ViIjoiYWRtaW4iLCJuYmYiOjE1OTkyMDEwNjYsImF6cCI6IkdRdml6S01ZMU5ZOG9TNmc4ek5QQjdraGp5c2EiLCJhbXIiOlsiQmFzaWNBdXRoZW50aWNhdG9yIl0sImlzcyI6Imh0dHBzOlwvXC8xOTIuMTY4LjEwLjE0Mzo5NDQzXC9vYXV0aDJcL3Rva2VuIiwiZXhwIjoxNTk5MjA0NjY2LCJpYXQiOjE1OTkyMDEwNjZ9.ix65WuCrZ24M8kqq4viWPKzyI6qxSNz__8s7d1CIaYhvvoa_mFNcXjM0IyPnpG8aEfDPmEPxgWYo4xE0HqlQVJvkTyOpQlv70FnpW-UrBHsZ1DV1MWj8Q83cAL9L68i6FGbYZon0x5qbkwaPKxCoA2r6nnY53g5o_IYDcVFN--KOz4jXNywcngXCBVszINbZ3V4_yB2T0Yg_u_0_1pl85hhzplMH3zEqOYdxR7byeWJyTqiiGAc7aJ696eqxrsiZ4dAy5jhkBP5_h7SFce2u1t-jtWIBaq4aKmXx0GLjjhOHT8h_OjlZGfczTaYLOWnTy35oM3OEhTU0iXx7yzavEA"},'
      +'"rights":['
      +'{"rightId":1,"parentRightId":0,"rightCode":"PaymentGateway","rightName":"Payment Gateway","status":1,"rightOrder":1,"hasChild":0,"urlRewrite":"/unit","iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
      +'{"rightId":2,"parentRightId":0,"rightCode":"Merchant","rightName":"Merchant Managerment","status":2,"rightOrder":2,"hasChild":0,"urlRewrite":"/app-status","iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
      +'{"rightId":3,"parentRightId":0,"rightCode":"Bank Management","rightName":"Bank Management","status":1,"rightOrder":3,"hasChild":1,"urlRewrite":null,"iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
      +'{"rightId":4,"parentRightId":0,"rightCode":"Statistics","rightName":"Statistics","status":1,"rightOrder":4,"hasChild":1,"urlRewrite":"/api","iconUrl":"fa fa-bar-chart","description":null,"applicationId":0,"accesses":[]},'
      +'{"rightId":5,"parentRightId":4,"rightCode":"Session","rightName":"Session","status":1,"rightOrder":2,"hasChild":0,"urlRewrite":"/application","iconUrl":"fa fa-angle-right","description":null,"applicationId":0,"accesses":[]}'
      +']}');


    this.countdownTimer();
  }

  logout() {
    // clear cache
    localStorage.clear()
    // redirect to wso2is

  }

  onSelectLanguage(lang: LanguageItem) {
    this.SelectedLanguage = lang;
    window.location.reload();
  }

  /**
   * @param expireTime the time tokens expire
   */
  countdownTimer() {
  }

  /**
   * @description Check for expired token
   * if token is expired, redirect to login page
   */


  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}

