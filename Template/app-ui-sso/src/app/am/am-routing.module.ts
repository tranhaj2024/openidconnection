import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmComponent } from './am.component';
import { AuthGuard } from '../authentication/guard/auth.guard';
const routes: Routes = [
  {
    path: '', component: AmComponent,
    canActivate: [AuthGuard],
    children: [
      // home application
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: './common/shared/home/home.module#HomeModule' },
      { path: 'session', loadChildren: './statistics/session/session.module#SessionModule' },
      { path: 'system-log', loadChildren: './statistics/system-log/system-log.module#SystemLogModule' },
      { path: 'receipt', loadChildren: './statistics/receipt/receipt.module#ReceiptModule' },
      { path: 'log-in-log', loadChildren: './statistics/log-in-log/log-in-log.module#LogInLogModule' },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmRoutingModule { }
