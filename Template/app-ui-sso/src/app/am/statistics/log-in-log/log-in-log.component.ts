import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DataTable} from 'angular2-datatable';
import {Http} from '@angular/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SearchObject} from './search-object';
import {AppConfig} from '../../../app.config';

@Component({
  selector: 'app-log-in-log',
  templateUrl: './log-in-log.component.html',
  styleUrls: ['./log-in-log.component.css'],
  providers: [DataTable]
})
export class LogInLogComponent implements OnInit {

  filterForm: FormGroup;
  filterObject: SearchObject = new SearchObject();
  checkAllItemFlag = false;
  currentPageView: number=0;
  totalPages: number=0;
  currentPage = 0;
  pageLength: number=0;
  totalElements: number=0;
  fromNumber: number=0;
  toNumber: number=0;
  numberDeleteItems: number;

  OrAPI = AppConfig.settings.API_URL;

  log: any;


  accountList: Array<any> ;
  acountListInfo: any;

  constructor(
    private translate: TranslateService,
    private http: Http,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.filterForm = this.fb.group({
      type_log: '',
      formDate: null,
      toDate: null,
      userLogin: '',
    });
    this.getListLoginLog(this.filterForm.value, 0);
  }

  /**
   * @description: Coordinator quản lý việc chuyển trang
   * @param page: số trang
   */
  choosePageNumber(page) {
    var flag = true;
    var pageNumber;
    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber) || !Number.isInteger(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        this.currentPageView = this.currentPage + 1;
      } else {
        pageNumber = page.valueAsNumber - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
      page.value = pageNumber + 1;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
      page.value = pageNumber + 1;

    }
    if (flag == true) {
      this.currentPage = pageNumber;
      this.currentPageView = this.currentPage + 1;
      this.getListLoginLog(this.filterObject, this.currentPage);
    }
  }

  /**
   * @description: Setting attribute để hiển thị phân trang.
   */
  private setCurrentPage(page) {

    if (page > 0) {
      this.currentPage = page;
    } else {
      this.currentPage = 0;
    }
    this.currentPageView = this.currentPage + 1;
    var size = this.accountList.length;
    this.fromNumber = this.currentPage * size + 1;
    this.toNumber = this.currentPageView * size;
    if (this.toNumber < 1) {
      this.fromNumber = 0;
      this.toNumber = 0;
    }
    this.pageLength = size;
    this.totalElements = this.acountListInfo.totalElements;
    this.totalPages = this.acountListInfo.totalPages;
    this.currentPage = page;
  }

  checkAllItem() {
    this.checkAllItemFlag = !this.checkAllItemFlag;
    this.accountList.forEach(item => {
      item.checked = this.checkAllItemFlag;
    });
  }

  countNumberDeleteItems() {
    this.numberDeleteItems = 0;
    this.accountList.forEach(item => {
      if (item.checked == true) {
        this.numberDeleteItems += 1;
      }
    });
  }


  getListLoginLog(searchObject: any, page: number) {
    this.filterObject = searchObject;
    this.http.get(this.OrAPI + '/list/searchLogin?search=' + encodeURI(JSON.stringify(searchObject)) + '&page=' + page + '&size=10',)
      .toPromise()
      .then(response => {
        this.acountListInfo = response.json().data;
        console.log(this.acountListInfo);
        this.accountList = response.json().data.content;
        console.log(this.accountList);

      }). then(() => {
      this.setCurrentPage(page);
    })
      .catch(error => {
        console.log(error);
      });
  }
}

