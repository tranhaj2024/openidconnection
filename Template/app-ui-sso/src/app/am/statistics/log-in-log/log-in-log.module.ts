import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogInLogComponent } from './log-in-log.component';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader, CustomHandler} from '../../../i18n-setting';
import {HttpClient} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {DataTableModule} from 'angular2-datatable';

const routes: Routes = [
  { path: '', component: LogInLogComponent, pathMatch: 'full' }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    DataTableModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomHandler },
      isolate: false
    }),
],
  declarations: [LogInLogComponent]
})
export class LogInLogModule { }
