class PaymentGates {
  gateId: number;
  gateName: string;
  gateCode: string;
  status: number;
  note: string;
  initUrl: string;
  queryUrl: string;
  doiSoat: string;
}

class PaymentMerchants {
  merchantsId: number;
  merchantCode: string;
  merchantName: string;
  status: number;
  note: string;

}

class PaymentGatesMerchants {
  id: number;
  gate:PaymentGates = new PaymentGates();
  merchant: PaymentMerchants = new PaymentMerchants();
  accessKey: string;
  secretKey: string;
  publicKey: string;
}

export class SearchObject {
    type_log: string;
    fromDate: string;
    toDate: string;
    userLogin:string
    orderid: string;
    ipAddress: string;
    dealid: PaymentGatesMerchants = new PaymentGatesMerchants();
    request_type: number;
    tradingCode: string;

}
