import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DataTable} from 'angular2-datatable';
import {SearchObject} from '../log-in-log/search-object';
import {Http} from '@angular/http';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {DialogService} from '../../common/dialog/dialog.service';
import {Toast, ToastOptions, ToastsManager} from 'ng2-toastr';
import {AppConfig} from '../../../app.config';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css'],
  providers: [DataTable, ToastsManager, ToastOptions]
})
export class SessionComponent implements OnInit {

  filterForm: FormGroup;
  checkAllItemFlag = false
  currentPageView: number=0;
  totalPages: number=0;
  currentPage = 0;
  pageLength: number=0;
  private datePipe: DatePipe;
  totalElements: number=0;
  fromNumber: number=0;
  toNumber: number=0;

  receiptList: Array<any>;
  receiptListInfo: any;
  numberDeleteItems: number;

  filterObject: SearchObject = new SearchObject();
  OrAPI = AppConfig.settings.API_URL;


  constructor(
    private translate: TranslateService,
    private http: Http,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private dialogService: DialogService,
    private fb: FormBuilder,
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  now = new Date();
  ngOnInit() {
    this.filterForm = this.fb.group({
      orderid: "",
      gateid: "",
      ipAddress: "",
      dealid: "",
      tradingCode: "",
      fromDate: "",
      toDate: "",
      fromTime: '01:00:00',
      toTime: '01:00:00',
    });

    this.search(this.filterForm, 0);
  }

  choosePageNumber(page) {
    var flag = true;
    var pageNumber;
    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber) || !Number.isInteger(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        this.currentPageView = this.currentPage + 1;
      } else {
        pageNumber = page.valueAsNumber - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
      page.value = pageNumber + 1;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
      page.value = pageNumber + 1;

    }
    if (flag == true) {
      this.currentPage = pageNumber;
      this.currentPageView = this.currentPage + 1;
      this.search(this.filterObject, this.currentPage);
    }
  }

  checkAllItem() {
    this.checkAllItemFlag = !this.checkAllItemFlag;
    this.receiptList.forEach(item => {
      item.checked = this.checkAllItemFlag;
    });
  }
  countNumberDeleteItems() {
    this.numberDeleteItems = 0;
    this.receiptList.forEach(item => {
      if (item.checked == true) {
        this.numberDeleteItems += 1;
      }
    });
  }


  search(searchObject: any, page: number) {
    this.convertObject(searchObject);
    console.log(this.filterObject);
    this.http.get(this.OrAPI + '/list/receptpaper?search=' + encodeURI(JSON.stringify(this.filterObject)) + '&page=' + page + '&size=10',)
      .toPromise()
      .then(response => {
        console.log(response);
        this.receiptListInfo = response.json().data;
        this.receiptList = response.json().data.content;
        console.log(this.receiptList);

      }). then(() => {
      this.setCurrentPage(page);
    })
      .catch(error => {
        console.log(error)
      });
  }

  deletePGML(id: any) : Promise<any> {
          var promise = this.http.delete(this.OrAPI + '/payment-gates-merchantslog?id=' + id)
            .toPromise()
            .then(response => {
              this.search(this.filterObject, 0);
            })

          return promise;
  }

  delete(id: any) {
    this.dialogService
      .confirm('Confirm Information', 'Are you sure to delete?')
      .subscribe(response => {
        if (response == true) {
          this.deletePGML(id)
            .then(response => {
              let message;
              this.translate.get('Message.DeleteSuccess').subscribe((res: string) => {
                message = res;
              });
              this.toastr.success('', message, { dismiss: 'controlled' })
                .then((toast: Toast) => {
                  setTimeout(() => {
                    this.toastr.dismissToast(toast);
                  }, 3000);
                });
              this.choosePageNumber(this.currentPage);
            })
            .catch(error => {
              let message;
              this.translate.get('Message.DeleteFail').subscribe((res: string) => {
                message = res;
              });
              this.toastr.error('', message, { dismiss: 'controlled' })
                .then((toast: Toast) => {
                  setTimeout(() => {
                    this.toastr.dismissToast(toast);
                  }, 3000);
                });
            });
        }
      })
  }

  deleteALLPGML(entityIds: any): Promise<any> {
      var promise = this.http.delete(this.OrAPI + '/list/receptpaper?ids=' + entityIds)
        .toPromise()
        .then(response => {
          this.search(this.filterObject, 0);
        })
      return promise;
  }


  deleteCheckedItems() {
    var entityIds = [];
    this.receiptList.forEach(item => {
      if (item.checked == true) {
        entityIds.push(item.id);
      }
    });
    console.log(this.receiptList);
    if (entityIds.length > 0) {
      this.dialogService.confirm('Confirm Information', 'Are you sure to delete?')
        .subscribe(response => {
          if (response == true) {
            this.deleteALLPGML(entityIds)
              .then(response => {
                this.checkAllItemFlag = false;
                this.choosePageNumber(this.currentPage);
                this.numberDeleteItems = 0;
                let message;
                this.translate.get('Message.DeleteSuccess').subscribe((res: string) => {
                  message = res;
                });
                this.toastr.success('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              })
              .catch(error => {
                this.numberDeleteItems = 0;
                this.checkAllItemFlag = false;
                this.choosePageNumber(this.currentPage);
                let message;
                this.translate.get('Message.DeleteFail').subscribe((res: string) => {
                  message = res;
                });
                this.toastr.error('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              });
          }
        })
    }
    this.countNumberDeleteItems();
  }



  private setCurrentPage(page) {

    if (page > 0) {
      this.currentPage = page;
    } else {
      this.currentPage = 0;
    }
    this.currentPageView = this.currentPage + 1;
    var size = this.receiptList.length;
    this.fromNumber = this.currentPage * size + 1;
    this.toNumber = this.currentPageView * size + page;
    if (this.toNumber < 1) {
      this.fromNumber = 0;
      this.toNumber = 0;
    }
    this.pageLength = size;
    this.totalElements = this.receiptListInfo.totalElements;
    this.totalPages = this.receiptListInfo.totalPages;
    this.currentPage = page;
  }


  export(opptionExport) {
    console.log(this.filterObject);
    window.location.href = this.OrAPI + '/list/receptpaper/export-file?search=' + encodeURI(JSON.stringify(this.filterObject)) + '&option=' + opptionExport
  }

  private convertObject(searchObject: any) {
    this.filterObject.tradingCode = searchObject.tradingCode;
    this.filterObject.orderid = searchObject.orderid;
    this.filterObject.ipAddress = searchObject.ipAddress;
    this.filterObject.dealid.gate.gateCode = searchObject.gateid;
    this.filterObject.fromDate = searchObject.fromDate == "" ? null : searchObject.fromDate;
    this.filterObject.toDate = searchObject.toDate == "" ? null : searchObject.toDate;
    this.filterObject.dealid.merchant.merchantCode = "";
  }



}
