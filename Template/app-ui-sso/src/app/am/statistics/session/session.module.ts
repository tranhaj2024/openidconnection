import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionComponent } from './session.component';
import {RouterModule} from '@angular/router';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader, CustomHandler} from '../../../i18n-setting';
import {HttpClient} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DataTableModule} from 'angular2-datatable';
import { SessionDetailComponent } from './session-detail/session-detail.component';
import {ToastModule} from 'ng2-toastr';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    ReactiveFormsModule,
    FormsModule,
    ToastModule.forRoot(),
    RouterModule.forChild([
      { path: '', component: SessionComponent, pathMatch: 'full' },
      { path: 'detail/:id', component: SessionDetailComponent, pathMatch: 'full' }
    ]),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: CustomHandler},
      isolate: false
    }),
  ],
  declarations: [SessionComponent, SessionDetailComponent]
})
export class SessionModule { }
