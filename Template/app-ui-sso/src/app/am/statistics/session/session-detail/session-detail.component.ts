import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Location } from '@angular/common';
import {Http} from '@angular/http';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-session-detail',
  templateUrl: './session-detail.component.html',
  styleUrls: ['./session-detail.component.css']
})
export class SessionDetailComponent implements OnInit {
  @Input() responseFors1: string = '';
  private sub: any;
  logdata : any;
  id : number


  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private location: Location,
    private http: Http,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.search(this.id)
    });
  }

  getStatus() {
    if(this.logdata.status == 0)
      return this.failStatus()
    return this.successStatus();
  }
  failStatus() {
    var fail = '';
    this.translate.get('Session.Detail.Status.Fail').subscribe(result => fail = result);
    return fail;
  }
  successStatus() {
    var success = '';
    this.translate.get('Session.Detail.Status.Success').subscribe(result => success = result);
    return success;
  }
  goBack() {
    this.location.back();
  }

  link = "http://localhost:6868/api/v1/savis/lgsp";

  search(id: number) {
    this.http.get(this.link + '/payment-gates-merchantslog?id=' + id,)
      .toPromise()
      .then(response => {
        console.log(response);
        this.logdata = response.json().data;
        console.log(this.logdata);

      })
      .catch(error => {
        console.log(error)
      });
  }
}
