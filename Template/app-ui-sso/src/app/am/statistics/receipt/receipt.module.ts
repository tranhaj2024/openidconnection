import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptComponent } from './receipt.component';
import {RouterModule} from '@angular/router';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader, CustomHandler} from '../../../i18n-setting';
import {HttpClient} from '@angular/common/http';
import { DataTableModule } from "angular2-datatable";
import {FormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    RouterModule.forChild([
      {path: '', component: ReceiptComponent, pathMatch: 'full'}
    ]),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: CustomHandler},
      isolate: false
    }),
    FormsModule,
  ],
  declarations: [ReceiptComponent]
})
export class ReceiptModule { }
