import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { DataTable } from 'angular2-datatable';
@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.css'],
  providers: [DataTable]
})
export class ReceiptComponent implements OnInit {
  checkAllItemFlag = false
  currentPageView: number=0;
  totalPages: number=0;
  currentPage = 0;
  pageLength: number=0;
  totalElements: number=0;
  fromNumber: number=0;
  toNumber: number=0;

  receiptList: Array<any> = [
    {"code":"00.H54SN01", "billName":"00.H54lgsp", "timePay":"2020-02-02" ,"pdfLink":"http://pdf","docLink":"http://doc", "checked":false},
    {"code":"00.H54SN01", "billName":"00.H54lgsp", "timePay":"2020-02-02","pdfLink":"http://pdf","docLink":"http://doc", "checked":false},
    {"code":"00.H54SN01", "billName":"00.H54lgsp", "timePay":"2020-02-02","pdfLink":"http://pdf","docLink":"http://doc", "checked":false},
    {"code":"00.H54SN01", "billName":"00.H54lgsp", "timePay":"2020-02-02","pdfLink":"http://pdf","docLink":"http://doc", "checked":false},
  ]
  numberDeleteItems: number;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {

  }

  choosePageNumber(page: number) {
    var flag = false;
    if (this.currentPage > page) {
      if (this.receiptList[0] == false) {
        flag = true;
      }
    } else if (this.currentPage < page) {
      if (this.receiptList[this.receiptList.length-1] == false) {
        flag = true;
      }
    } else {
      flag = true;
    }
    if (flag == true) {
      this.currentPage = page;
    }
  }
  checkAllItem() {
    this.checkAllItemFlag = !this.checkAllItemFlag;
    this.receiptList.forEach(item => {
      item.checked = this.checkAllItemFlag;
    });
  }
  countNumberDeleteItems() {
    this.numberDeleteItems = 0;
    this.receiptList.forEach(item => {
      if (item.checked == true) {
        this.numberDeleteItems += 1;
      }
    });
  }
  deleteCheckedItems() {

  }
}
