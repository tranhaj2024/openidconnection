import { Component, OnInit } from '@angular/core';
import {SearchObject} from '../log-in-log/search-object';
import {Http} from '@angular/http';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {AppConfig} from '../../../app.config';

@Component({
  selector: 'app-system-log',
  templateUrl: './system-log.component.html',
  styleUrls: ['./system-log.component.css'],
  providers: [DatePipe]
})
export class SystemLogComponent implements OnInit {

  filterForm: FormGroup;
  currentPageView: number=0;
  totalPages: number=0;
  currentPage = 0;
  pageLength: number=0;
  totalElements: number=0;
  fromNumber: number=0;
  toNumber: number=0;
  numberDeleteItems: number;
  filterObject: SearchObject = new SearchObject();
  OrAPI = AppConfig.settings.API_URL;
  logs: Array<any>;
  logsInfo: any;

  constructor(
    private http: Http,
    private fb: FormBuilder,
    private datePipe: DatePipe,
  ) { }

  ngOnInit() {
    let now = new Date();
    let last = new Date();
    let lastWeek = new Date(last.setMonth(last.getMonth() - 1));
    this.filterForm = this.fb.group({
      fromDate: this.datePipe.transform(lastWeek, 'yyyy-MM-dd'),
      toDate: this.datePipe.transform(now, 'yyyy-MM-dd'),
      fromTime: '01:00:01',
      toTime: this.datePipe.transform(now, 'HH:mm:ss'),
      request_type: 0,
    });

    this.search(this.filterForm, 0);
  }

  choosePageNumber(page) {
    var flag = true;
    var pageNumber;
    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber) || !Number.isInteger(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        this.currentPageView = this.currentPage + 1;
      } else {
        pageNumber = page.valueAsNumber - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
      page.value = pageNumber + 1;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
      page.value = pageNumber + 1;

    }
    if (flag == true) {
      this.currentPage = pageNumber;
      this.currentPageView = this.currentPage + 1;
      this.search(this.filterObject, this.currentPage);
    }
  }


  search(searchObject: any, page: number) {
    console.log(searchObject);
    this.convertObject(searchObject);
    console.log(this.filterObject);
    this.http.get(this.OrAPI + '/list/payment-log?search=' + encodeURI(JSON.stringify(this.filterObject)) + '&page=' + page + '&size=10',)
      .toPromise()
      .then(response => {
        console.log(response);
        this.logsInfo = response.json().data;
        this.logs = response.json().data.content;
        console.log(this.logs);
        console.log(this.logsInfo);

      }). then(() => {
      this.setCurrentPage(page);
    })
      .catch(error => {
        console.log(error)
      });
  }

  private convertObject(searchObject: any) {
    this.filterObject.fromDate = searchObject.fromDate;
    this.filterObject.toDate = searchObject.toDate;
    this.filterObject.request_type = searchObject.request_type;
  }


  private setCurrentPage(page) {

    if (page > 0) {
      this.currentPage = page;
    } else {
      this.currentPage = 0;
    }
    this.currentPageView = this.currentPage + 1;
    var size = this.logs.length;
    this.fromNumber = this.currentPage * size + 1;
    this.toNumber = this.currentPageView * size ;
    if (this.toNumber < 1) {
      this.fromNumber = 0;
      this.toNumber = 0;
    }
    this.pageLength = size;
    this.totalElements = this.logsInfo.totalElements;
    this.totalPages = this.logsInfo.totalPages;
    this.currentPage = page;
  }


}
