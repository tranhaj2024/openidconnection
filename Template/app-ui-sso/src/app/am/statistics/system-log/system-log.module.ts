import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { SystemLogComponent } from './system-log.component';
import {RouterModule, Routes} from '@angular/router';
import {DataTableModule} from 'angular2-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {createTranslateLoader, CustomHandler} from '../../../i18n-setting';
import {HttpClient} from '@angular/common/http';
import {DateTimePickerModule} from 'ng-pick-datetime';

const routes: Routes = [
  { path: '', component: SystemLogComponent, pathMatch: 'full' }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    DataTableModule,
    ReactiveFormsModule,
    DateTimePickerModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomHandler },
      isolate: false
    }),
  ],
  declarations: [SystemLogComponent]
})
export class SystemLogModule { }
