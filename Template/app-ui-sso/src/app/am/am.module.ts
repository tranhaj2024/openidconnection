import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmComponent } from './am.component';
import { AmRoutingModule } from './am-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MdDialogModule, MdButtonModule } from '@angular/material';
import { DialogComponent } from './common/dialog/dialog.component';
import { DialogService } from './common/dialog/dialog.service';
import { DialogEmptyComponent } from './common/dialog-empty/dialog-empty.component';
import { DialogEmptyService } from './common/dialog-empty/dialog-empty.service';
import { SidebarComponent } from './common/shared/sidebar/sidebar.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateCompiler, TranslateParser } from '@ngx-translate/core';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import { CustomHandler, createTranslateLoader } from '../i18n-setting';
import { HttpClient } from '@angular/common/http';
// import { ResponseForTableComponent } from './categories/send_edoc/response-for/response-for-table.component'
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { AuthGuard } from '../authentication/guard/auth.guard';
import {AuthenticationService} from '../authentication/guard/authentication.service';
@NgModule({
  imports: [
    AmRoutingModule,
    FormsModule,
    HttpModule,
    MdDialogModule,
    MdButtonModule,
    ReactiveFormsModule,
    CommonModule,
    AmRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomHandler },
      isolate: false
    }),
    MomentModule,
    NgIdleKeepaliveModule.forRoot()
  ],
  entryComponents: [DialogComponent,
    DialogEmptyComponent
  ],
  exports: [DialogComponent],
  providers: [
    DialogService,
    DialogEmptyService,
    AuthGuard,
    AuthenticationService,
    { provide: LocationStrategy, useClass: PathLocationStrategy }
    ],
  declarations: [AmComponent, DialogComponent, SidebarComponent,
     DialogEmptyComponent
    ],
    // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class AmModule { }
