import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DialogEmptyComponent } from './dialog-empty.component';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';


@Injectable()
export class DialogEmptyService {

  constructor(private dialog: MdDialog) { }

  public confirm(title: string, message: string): Observable<boolean> {

    let dialogRef: MdDialogRef<DialogEmptyComponent>;

    dialogRef = this.dialog.open(DialogEmptyComponent);

    dialogRef.componentInstance.emptyTitle = title;
    dialogRef.componentInstance.emptyMessage = message;

    return dialogRef.afterClosed();
  }

}
