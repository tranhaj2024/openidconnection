import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
@Component({
  selector: 'app-dialog-empty',
  templateUrl: './dialog-empty.component.html',
  styleUrls: ['./dialog-empty.component.css']
})
export class DialogEmptyComponent implements OnInit {

  public emptyTitle: string;
  public emptyMessage: string;

  constructor(public dialogRef: MdDialogRef<DialogEmptyComponent>) {

  }

  ngOnInit() {
  }

}
