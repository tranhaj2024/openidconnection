export class SynchronizeObject{
    id: number;
    tableName: string;
    lastSuccess: Date;
    status: string;
    checked: boolean
    startDate: Date
    endDate: Date
}