import { Injectable } from "@angular/core";
@Injectable()
export class ExcelExportService {
    constructor() {}

    static toExportFileName(excelFileName: string): string {
      return `${excelFileName}_${new Date().getDate()}.xlsx`;
    }
  
    public exportAsExcelFile(json: any[], excelFileName: string): void {
    }
}