import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [TranslateService]
})
export class HomeComponent implements OnInit {

  ngOnInit() {
  }


}

class BarChartDataItem {
  name: string;
  y: number;
}


