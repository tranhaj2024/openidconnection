import { Component, OnInit, AfterViewInit, Input, AfterViewChecked } from '@angular/core';
import { Right } from './right';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserInfo } from '../user-info/user-info';
declare var Layout, App: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [TranslateService]
})
export class SidebarComponent implements OnInit, AfterViewInit, AfterViewChecked {

  rights: Array<Right> = [];
  rootNavations: Right[];
  userInfo: UserInfo;
  constructor(
    private router: Router,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.getMenuRightByCurrentUser();
  }

  ngAfterViewInit() {
    Layout.init();
  }

  setRightsName() {
    for (let i = 0; i < this.rights.length; i++) {
      for(let j=i+1;j<this.rights.length; j++) {
        if(this.rights[i].rightOrder > this.rights[j].rightOrder) {
          var right = this.rights[i];
          this.rights[i]=this.rights[j];
          this.rights[j]=right;
        }
      }
      let rightNameTranlate;
      let rightNameTranlateKey = 'SIDEBAR.' + this.rights[i].rightName;
      this.translate.get(rightNameTranlateKey).subscribe((res: string) => {
        rightNameTranlate = res;
        this.rights[i].rightName = rightNameTranlate;
      });
    }
  }

  sortRight(a:Right, b:Right):number {
    return a.rightOrder < b.rightOrder?0:1;
  }


  ngAfterViewChecked() {
    //App.init();

  }

  getChildRights(parentRightId: number): Right[] {
    let childRights = [];
    let totalRights = this.rights.length;
    for (let i = 0; i < totalRights; i++) {
      var item = this.rights[i];
      if (item.parentRightId == parentRightId) {
        childRights.push(item);
      }
    }
    return childRights;
  }

  getMenuRightByCurrentUser() {

    try {
      this.userInfo = JSON.parse('{'
      +'"userName":"admin",'
      +'"loweredUsername":"admin",'
      +'"mobileAlias":"0988888888",'
      +'"isAnonymous":0,"userType":0,'
      +'"applicationId":0,'
      +'"accessTokenInfo":{'
        +'"accessToken":"688210ac-c271-38e9-ba62-19f474d5b315",'
        +'"refreshToken":"5411a4d6-3d2d-32d6-8113-08473c1fed04",'
        +'"expiresIn":3600,'
        +'"idToken":"eyJ4NXQiOiJOVEF4Wm1NeE5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJraWQiOiJOVEF4Wm1NeE5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiVHg2Qk54TG5PN01hTUdZZ0xtMDhLZyIsImF1ZCI6IkdRdml6S01ZMU5ZOG9TNmc4ek5QQjdraGp5c2EiLCJjX2hhc2giOiJiYUo0Zm11TlRVa3lYcEsyRVpvVWt3Iiwic3ViIjoiYWRtaW4iLCJuYmYiOjE1OTkyMDEwNjYsImF6cCI6IkdRdml6S01ZMU5ZOG9TNmc4ek5QQjdraGp5c2EiLCJhbXIiOlsiQmFzaWNBdXRoZW50aWNhdG9yIl0sImlzcyI6Imh0dHBzOlwvXC8xOTIuMTY4LjEwLjE0Mzo5NDQzXC9vYXV0aDJcL3Rva2VuIiwiZXhwIjoxNTk5MjA0NjY2LCJpYXQiOjE1OTkyMDEwNjZ9.ix65WuCrZ24M8kqq4viWPKzyI6qxSNz__8s7d1CIaYhvvoa_mFNcXjM0IyPnpG8aEfDPmEPxgWYo4xE0HqlQVJvkTyOpQlv70FnpW-UrBHsZ1DV1MWj8Q83cAL9L68i6FGbYZon0x5qbkwaPKxCoA2r6nnY53g5o_IYDcVFN--KOz4jXNywcngXCBVszINbZ3V4_yB2T0Yg_u_0_1pl85hhzplMH3zEqOYdxR7byeWJyTqiiGAc7aJ696eqxrsiZ4dAy5jhkBP5_h7SFce2u1t-jtWIBaq4aKmXx0GLjjhOHT8h_OjlZGfczTaYLOWnTy35oM3OEhTU0iXx7yzavEA"},'
        +'"rights":['
        +'{"rightId":1,"parentRightId":0,"rightCode":"PaymentGateway","rightName":"Payment Gateway","status":1,"rightOrder":1,"hasChild":0,"urlRewrite":"/payment-gateway","iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":2,"parentRightId":0,"rightCode":"MerchantManagement","rightName":"Merchant Management","status":2,"rightOrder":2,"hasChild":0,"urlRewrite":"/merchant-management","iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":3,"parentRightId":0,"rightCode":"Bank Management","rightName":"Bank Management","status":1,"rightOrder":3,"hasChild":1,"urlRewrite":"bank-management","iconUrl":"fa fa-cog","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":4,"parentRightId":0,"rightCode":"Statistics","rightName":"Statistics","status":1,"rightOrder":4,"hasChild":1,"urlRewrite":"/statistic","iconUrl":"fa fa-bar-chart","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":5,"parentRightId":4,"rightCode":"Receipt","rightName":"Receipt","status":1,"rightOrder":1,"hasChild":0,"urlRewrite":"/receipt","iconUrl":"fa fa-angle-right","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":6,"parentRightId":4,"rightCode":"Session","rightName":"Session","status":1,"rightOrder":2,"hasChild":0,"urlRewrite":"/session","iconUrl":"fa fa-angle-right","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":7,"parentRightId":4,"rightCode":"LogInLog","rightName":"Log In Log","status":1,"rightOrder":3,"hasChild":0,"urlRewrite":"/log-in-log","iconUrl":"fa fa-angle-right","description":null,"applicationId":0,"accesses":[]},'
        +'{"rightId":8,"parentRightId":4,"rightCode":"SystemLog","rightName":"System Log","status":1,"rightOrder":4,"hasChild":0,"urlRewrite":"/system-log","iconUrl":"fa fa-angle-right","description":null,"applicationId":0,"accesses":[]}'
        +']}' )
      this.rights = this.userInfo.rights;
      console.log(this.userInfo)
      this.setRightsName();

    } catch (element) {
    }
  }
}
