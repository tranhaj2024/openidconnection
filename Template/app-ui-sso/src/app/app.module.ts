import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { MdDialogModule, MdButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateCompiler, TranslateParser } from '@ngx-translate/core';
import {  CustomHandler, createTranslateLoader } from './i18n-setting';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {AuthRoutingModule} from './authentication/auth-routing.module';
import {AppConfig} from './app.config';
import {EmployeeService} from './login/employee.service';
import {LoginComponent} from './login/login.component';


export function initializeApp(appConfig: AppConfig) {
  return () => appConfig.load();
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomHandler },
      isolate: false
    }),
    FormsModule,
    HttpModule,
    MdDialogModule,
    MdButtonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AuthRoutingModule,
  ],
  providers: [TranslateService,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    AppConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfig], multi: true
    },
    EmployeeService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }

